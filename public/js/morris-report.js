var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
$(function() {

    Morris.Line({
        element: 'hotspot-aktif',
        data:[
         {
            "month":"2015-01",
            "jakpus":"23",
            "jakbar":"19",
            "jaksel":"15",
            "jaktim":"45",
            "jakut":"45",
            "kepsri":"2"
         },
         {
            "month":"2015-02",
            "jakpus":"23",
            "jakbar":"20",
            "jaksel":"15",
            "jaktim":"45",
            "jakut":"45",
            "kepsri":"2"
         },
         {
            "month":"2015-03",
            "jakpus":"23",
            "jakbar":"20",
            "jaksel":"15",
            "jaktim":"45",
            "jakut":"45",
            "kepsri":"2"
         },
         {
            "month":"2015-04",
            "jakpus":"23",
            "jakbar":"21",
            "jaksel":"16",
            "jaktim":"45",
            "jakut":"45",
            "kepsri":"3"
         },
         {
            "month":"2015-05",
            "jakpus":"24",
            "jakbar":"21",
            "jaksel":"17",
            "jaktim":"46",
            "jakut":"46",
            "kepsri":"3"
         },
         {
            "month":"2015-06",
            "jakpus":"24",
            "jakbar":"20",
            "jaksel":"17",
            "jaktim":"46",
            "jakut":"46",
            "kepsri":"3"
         },
         {
            "month":"2015-07",
            "jakpus":"19",
            "jakbar":"18",
            "jaksel":"10",
            "jaktim":"39",
            "jakut":"39",
            "kepsri":"1"
         },
         {
            "month":"2015-08",
            "jakpus":"25",
            "jakbar":"22",
            "jaksel":"17",
            "jaktim":"46",
            "jakut":"46",
            "kepsri":"3"
         },
         {
            "month":"2015-09",
            "jakpus":"25",
            "jakbar":"22",
            "jaksel":"17",
            "jaktim":"46",
            "jakut":"46",
            "kepsri":"3"
         },
         {
            "month":"2015-10",
            "jakpus":"26",
            "jakbar":"22",
            "jaksel":"17",
            "jaktim":"46",
            "jakut":"46",
            "kepsri":"3"
         },
         {
            "month":"2015-11",
            "jakpus":"27",
            "jakbar":"23",
            "jaksel":"18",
            "jaktim":"46",
            "jakut":"46",
            "kepsri":"3"
         },
         {
            "month":"2015-12",
            "jakpus":"27",
            "jakbar":"23",
            "jaksel":"18",
            "jaktim":"47",
            "jakut":"47",
            "kepsri":"3"
         }
        ],
        xkey: 'month',
        ykeys: ['jakpus','jakbar','jaksel','jaktim','jakut','kepsri'],
        labels: ['Jakarta Pusat','Jakarta Barat','Jakarta Selatan','Jakarta Timur','Jakarta Utara', 'Kep. Seribu' ],
        xLabelFormat: function(x) {
        var month = months[x.getMonth()];
        return month;
        },
        dateFormat: function(x) {
        var month = months[new Date(x).getMonth()];
        return month;
        },
        hideHover: 'auto',
        resize: true
    });

    Morris.Bar({
        element: 'hotspot-baru',
        data:[
          {
            "month":"2015-01",
            "jakpus":"10",
            "jakbar":"9",
            "jaksel":"8",
            "jaktim":"12",
            "jakut":"12",
            "kepsri":"2"
         },
         {
            "month":"2015-02",
            "jakpus":"7",
            "jakbar":"5",
            "jaksel":"12",
            "jaktim":"18",
            "jakut":"12",
            "kepsri":"1"
         },
         {
            "month":"2015-03",
            "jakpus":"2",
            "jakbar":"1",
            "jaksel":"2",
            "jaktim":"2",
            "jakut":"1",
            "kepsri":"2"
         },
         {
            "month":"2015-04",
            "jakpus":"0",
            "jakbar":"2",
            "jaksel":"1",
            "jaktim":"0",
            "jakut":"1",
            "kepsri":"2"
         },
         {
            "month":"2015-05",
            "jakpus":"1",
            "jakbar":"2",
            "jaksel":"2",
            "jaktim":"1",
            "jakut":"0",
            "kepsri":"2"
         },
         {
            "month":"2015-06",
            "jakpus":"1",
            "jakbar":"2",
            "jaksel":"1",
            "jaktim":"1",
            "jakut":"2",
            "kepsri":"1"
         },
         {
            "month":"2015-07",
            "jakpus":"0",
            "jakbar":"0",
            "jaksel":"0",
            "jaktim":"0",
            "jakut":"0",
            "kepsri":"0"
         },
         {
            "month":"2015-08",
            "jakpus":"2",
            "jakbar":"2",
            "jaksel":"0",
            "jaktim":"2",
            "jakut":"0",
            "kepsri":"1"
         },
         {
            "month":"2015-09",
            "jakpus":"2",
            "jakbar":"1",
            "jaksel":"1",
            "jaktim":"1",
            "jakut":"2",
            "kepsri":"1"
         },
         {
            "month":"2015-10",
            "jakpus":"1",
            "jakbar":"2",
            "jaksel":"0",
            "jaktim":"0",
            "jakut":"2",
            "kepsri":"0"
         },
         {
            "month":"2015-11",
            "jakpus":"0",
            "jakbar":"0",
            "jaksel":"1",
            "jaktim":"1",
            "jakut":"0",
            "kepsri":"2"
         },
         {
            "month":"2015-12",
            "jakpus":"1",
            "jakbar":"2",
            "jaksel":"2",
            "jaktim":"1",
            "jakut":"0",
            "kepsri":"2"
         }
        ],
        xkey: 'month',
        ykeys: ['jakpus','jakbar','jaksel','jaktim','jakut','kepsri'],
        labels: ['Jakarta Pusat','Jakarta Barat','Jakarta Selatan','Jakarta Timur','Jakarta Utara', 'Kep. Seribu' ],
        // xLabels : months,
        xLabels: function(x) {
        var month = months[x.getMonth()];
        return month;
        },
        dateFormat: function(x) {
        var month = months[new Date(x).getMonth()];
        return month;
        },
        hideHover: 'auto',
        resize: true
    });
    // Morris.Donut({
    //     element: 'morris-donut-chart',
    //     data: [{
    //         label: "Download Sales",
    //         value: 12
    //     }, {
    //         label: "In-Store Sales",
    //         value: 30
    //     }, {
    //         label: "Mail-Order Sales",
    //         value: 20
    //     }],
    //     resize: true
    // });
    //
    // Morris.Bar({
    //     element: 'morris-bar-chart',
    //     data: [{
    //         y: '2006',
    //         a: 100,
    //         b: 90
    //     }, {
    //         y: '2007',
    //         a: 75,
    //         b: 65
    //     }, {
    //         y: '2008',
    //         a: 50,
    //         b: 40
    //     }, {
    //         y: '2009',
    //         a: 75,
    //         b: 65
    //     }, {
    //         y: '2010',
    //         a: 50,
    //         b: 40
    //     }, {
    //         y: '2011',
    //         a: 75,
    //         b: 65
    //     }, {
    //         y: '2012',
    //         a: 100,
    //         b: 90
    //     }],
    //     xkey: 'y',
    //     ykeys: ['a', 'b'],
    //     labels: ['Series A', 'Series B'],
    //     hideHover: 'auto',
    //     resize: true
    // });

});
