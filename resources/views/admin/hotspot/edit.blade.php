@extends('layouts.admin')

@section('content')

<script type="text/javascript">
    $(function() {
        $('#idKota').change(function() {
            var kota_id = $('#idKota').val();
            var token =  $("input[name=_token]").val();
            $.ajax({
                url: '{!! route('admin.hotspot.getkecamatan') !!}',
                method: 'POST',
                dataType: 'json',
                data: { _token: token, kota_id: '{{ $data->kota_id }}', kecamatan_id: '{{ $data->kecamatan_id }}' },
                success: function(data) {
                    $('#idKecamatan').html(data);
                    $('#idKecamatan').focus();
                }
            });
        }).change();

        $('#idKecamatan').change(function() {
        	var kecamatan_id = $('#idKecamatan').val();
        	var token = $("input[name=_token]").val();
        	$.ajax({
        		url: '{!! route('admin.hotspot.getkelurahan') !!}',
        		method: 'POST',
        		dataType: 'json',
        		data: { _token:token, kecamatan_id:'{{ $data->kecamatan_id }}', kelurahan_id:'{{ $data->kelurahan_id }}' },
        		success: function(data) {
        			$('#idKelurahan').html(data);
        			$('#idKelurahan').focus();
        		}
        	});
        }).change();
    });
</script>


<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header">Hotspot</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
          	<div class="panel-heading">Ubah Hotspot</div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                  	<div class="row">
                  <div class="col-md-12">
                      @include('partials.alerts.errors')
                  </div>
                        <div class="col-lg-6">
                            {!! Form::model($data, ['method'=>'PATCH', 'route' => ['admin.hotspot.update', $data->id]]) !!}
                              	<div class="form-group">
                              		{!! Form::label('nama', 'Nama Hotspot') !!}
                              		{!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
                              	</div>
                              	<div class="form-group">
                              		{!! Form::label('kota', 'Kota') !!}
                              		{!! Form::select('kota_id', $kota, Input::old('kota_id'), ['class'=>'form-control', 'id'=>'idKota']) !!}
                              	</div>
                              	<div class="form-group">
                              		{!! Form::label('kecamatan', 'Kecamatan') !!}
                              		{!! Form::select('kecamatan_id', array(), Input::old('kecamatan_id'), ['class'=>'form-control', 'id'=>'idKecamatan']) !!}
                              	</div>
                              	<div class="form-group">
                              		{!! Form::label('kelurahan', 'Kelurahan') !!}
                              		{!! Form::select('kelurahan_id', array(), Input::old('kelurahan_id'), ['class'=>'form-control', 'id'=>'idKelurahan']) !!}
                              	</div>
                              	<div class="form-group">
                              		{!! Form::label('jenis', 'Tipe Hotspot') !!}
                              		{!! Form::select('tipe', $tipe, Input::old('tipe'), ['class'=>'form-control']) !!}
                              	</div>
                            	<div>
                            		{!! Form::button('Simpan', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
                                	<a href="{{ URL::route('admin.hotspot.index') }}" type="button" class="btn btn-danger">Batal</a>
                            	</div>
                        	{!! Form::close() !!}
                      	</div>
                      	<div class="col-lg-6"></div>
                        <!-- /.table-responsive -->
                    </div>
                   	<!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

@stop