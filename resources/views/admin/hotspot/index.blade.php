@extends('layouts.admin')

@section('content')
<script type="text/javascript">
	$(function() {
    	$('#hotspot-table').DataTable({
        	processing: true,
        	serverSide: true,
        	ajax: '{!! route('admin.hotspot.data') !!}',
        	columns: [
            	{ data: '0', name: 'id', orderable: false, searchable: false },
            	{ data: '1', name: 'name' },
            	{ data: '2', name: 'kota_id' },
            	{ data: '3', name: 'kecamatan_id' },
            	{ data: '4', name: 'kelurahan_id' },
            	{ data: '5', name: 'tipe' },
            	{ data: '6', name: 'created_at' },
            	{ data: '7', name: 'updated_at' },
            	{ data: '8', name: 'action', orderable: false, searchable: false }
        	]
    	});

      $('#hotspot-table').DataTable().on('click', '.btn-delete[data-remote]', function (e) {
        $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
        
        var x = confirm('Apakah Anda ingin menghapus data ini?');
        if (x) {
            e.preventDefault();
            var url = $(this).data('remote');
            // confirm then
            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                data: { method: '_DELETE', submit:true }
            }).always(function (data) {
              $('#hotspot-table').DataTable().draw(false);
            });
        } else {
          return false;
        }
      });

	});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Hotspot</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <!-- /.panel-heading -->
            <div class="panel-body">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('flash_message') !!}
                    </div>
                @endif
                <div class="dataTable_wrapper">
                  	<table class="table table-striped table-hover table-condensed" id="hotspot-table" >
                        <thead>
                    		<tr>
                              	<th></th>
                              	<th>Nama</th>
                              	<th>Kota</th>
                              	<th>Kecamatan</th>
                              	<th>Kelurahan</th>
                              	<th>Jenis Hotspot</th>
                              	<th>Created</th>
                              	<th>Updated</th>
                              	<th>Action</th>
                    		</tr>
                        </thead>
                  	</table>
                </div>
                <div>
                	<a href="{{ route('admin.hotspot.create') }}" class="btn btn-info">Tambah Hotspot</a>
            	</div>

            </div>
        </div>
        <!-- /.table-responsive -->

    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<script type="text/javascript">
	$(document).ready(function() {
		$(".delete").on("submit", function() {
			return confirm('Apakah benar ingin menghapus data ini?');
		});
	});
</script>

@stop