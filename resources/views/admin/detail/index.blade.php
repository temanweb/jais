@extends('layouts.admin')

@section('content')
<script type="text/javascript">
$(function() {
    $('#hotspot-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.hotspot.data') !!}',
        columns: [
            { data: '0', name: 'id', orderable: false, searchable: false },
            { data: '1', name: 'name' },
            { data: '2', name: 'kota_id' },
            { data: '3', name: 'kecamatan_id' },
            { data: '4', name: 'kelurahan_id' },
            { data: '5', name: 'tipe' },
            { data: '6', name: 'user_id' },
            { data: '7', name: 'created_at' },
            { data: '8', name: 'updated_at' },
            { data: '9', name: 'action', orderable: false, searchable: false }
        ]
    });

});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Hotspot</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="custom-alert"></div>
                <div class="dataTable_wrapper">
                  	<table class="table table-striped table-hover table-condensed" id="hotspot-table" >
                        <thead>
                    		<tr>
                              	<th></th>
                              	<th>Nama</th>
                              	<th>Kota</th>
                              	<th>Kecamatan</th>
                              	<th>Kelurahan</th>
                              	<th>Jenis Hotspot</th>
                              	<th>User</th>
                              	<th>Created</th>
                              	<th>Updated</th>
                              	<th>Action</th>
                    		</tr>
                        </thead>
                  	</table>
                </div>
                <div>
                	<a href="add_hotspot.html" class="btn btn-info">Tambah Hotspot</a>
            	</div>

            </div>
        </div>
        <!-- /.table-responsive -->

    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@stop