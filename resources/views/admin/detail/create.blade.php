@extends('layouts.admin')

@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Hotspot</h1>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Tambah Laporan Hotspot
      </div><!-- /.panel-heading -->
      <div class="panel-body">
        <div class="row">
                <div class="col-md-12">
                    @include('partials.alerts.errors')
                </div>
          <div class="col-lg-12">
            {!! Form::open(['route'=>'admin.detail.store']) !!}
              <div class="form-group">
                {!! Form::hidden('id', $id, ['class'=>'form-control']) !!}
              </div>
              <div class="form-group">
                {!! Form::label('estimasi_jumlah_populasi', 'Estimasi Jumlah Populasi') !!}
                {!! Form::text('estimasi_jumlah_populasi', Input::old('estimasi_jumlah_populasi'), ['class'=>'form-control']) !!}
              </div>
              <div class="form-group">
                {!! Form::label('estimasi_jumlah_populasi_baru', 'Estimasi Jumlah Populasi Baru') !!}
                {!! Form::text('estimasi_jumlah_populasi_baru', Input::old('estimasi_jumlah_populasi_baru'), ['class'=>'form-control']) !!}
              </div>
              <div class="form-group">
                {!! Form::label('estimasi_populasi_berpindah', 'Estimasi Populasi Berpindah') !!}
                {!! Form::text('estimasi_populasi_berpindah', Input::old('estimasi_populasi_berpindah'), ['class'=>'form-control']) !!}
              </div>
              <div class="form-group">
                {!! Form::label('populasi_terjangkau', 'Populasi Terjangkau') !!}
                {!! Form::text('populasi_terjangkau', Input::old('populasi_terjangkau'), ['class'=>'form-control']) !!}
              </div>
				<div class="form-group">
					{!! Form::label('status', 'Status') !!}
					{!! Form::select('status', array(''=>'Pilih', '1'=>'Aktif', '0'=>'Tidak Aktif'), Input::old('status'), ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('jenis', 'Jenis') !!}
					{!! Form::select('jenis', array(''=>'Pilih', '1'=>'Aktif', '0'=>'Tidak Aktif'), Input::old('jenis'), ['class'=>'form-control']) !!}
				</div>
              <div class="form-group">
                {!! Form::label('notes', 'Notes') !!}
                {!! Form::textarea('notes', Input::old('notes'), ['class'=>'form-control', 'rows'=>'2']) !!}
              </div>
              <div>
                {!! Form::button('Simpan', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
                <a href="{{ URL::previous() }}" class="btn btn-danger">
                            <i class="icon ti-back-left"></i><span>Kembali</span>
                        </a>
              </div>
              {!! Form::close() !!}
          </div>
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
</div><!-- /#page-wrapper -->


@stop