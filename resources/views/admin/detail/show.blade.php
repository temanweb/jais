@extends('layouts.admin')

@section('content')
<script type="text/javascript" class="init">
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table class="rowdetails">'+
            '<tr>'+
                '<td>Catatan&nbsp; </td>'+
                '<td>:&nbsp;'+d[8]+'</td>'+
            '</tr>'+

        '</table>';
    }

  $(function() {
      var table = $('#hotspot-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
            url: '{!! route('admin.detail.data') !!}',
            data: { 'id': '{{ $id }}' }
          },
          columns: [
            { data: '0', name: 'id', orderable: false, searchable: false },
            { data: '1', name: 'updated_at' },
            { data: '2', name: 'estimasi_populasi' },
            { data: '3', name: 'populasi_berpindah' },
            { data: '4', name: 'populasi_baru' },
            { data: '5', name: 'populasi_terjangkau' },
            { data: '6', name: 'hotspot_id' },
            { data: '7', name: 'status' },
            { data: '9', name: 'action', orderable: false, searchable: false }
          ],
          order: []
      });
      table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        });
      }).draw();

      $('#hotspot-table').on('click', 'button', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
      });

      $('#hotspot-table').DataTable().on('click', '.btn-delete[data-remote]', function (e) {
        $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
        
        var x = confirm('Apakah Anda ingin menghapus data ini?');
        if (x) {
            e.preventDefault();
            var url = $(this).data('remote');
            // confirm then
            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                data: { method: '_DELETE', submit:true }
            }).always(function (data) {
              $('#hotspot-table').DataTable().draw(false);
            });
        } else {
          return false;
        }
      });

  });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Hotspot</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
          	<div class="panel-heading">Detail Hotspot</div>

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                        	<dt>Nama Hotspot</dt><br/>
                            <dt>Kota</dt><br/>
                            <dt>Kecamatan</dt><br/><br/>
                      	</div>
                        <div class="col-md-4">
                        	<dd>:&nbsp;{{ $output['name'] }}</dd><br/>
                    		  <dd>:&nbsp;{{ $output['kota']['name'] }}</dd><br/>
                        	<dd>:&nbsp;{{ $output['kecamatan']['name'] }}</dd><br/><br/>
                        </div>
                      	<div class="col-md-2">
                        	<dt>Kelurahan</dt><br/>
                            <dt>Status Terakhir</dt><br/>
                            <dt>Jenis Hotspot</dt><br/><br/>
                      	</div>
                    	<div class="col-md-4">
                          	<dd>:&nbsp;{{ $output['kelurahan']['name'] }}</dd><br/>
                          	<dd>:&nbsp;Aktif</dd><br/>
                          	<dd>:&nbsp;{{ $output['tipe_hotspot']['name'] }}</dd><br/><br/>
                        </div>
                    </div>
                	<div class="row">
                        <div class="col-lg-12">
                          @if(Session::has('flash_message'))
                              <div class="alert alert-success">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  {!! Session::get('flash_message') !!}
                              </div>
                          @endif
                        </div>
                      	<div class="col-lg-12">
                      		<div class="panel-group">
                          		<div class="panel panel-default">
                              		<div class="panel-heading">Laporan</div>
                              		<div class="panel-body">
                              			<div class="dataTable_wrapper">
                              				<table class="table table-striped table-hover table-condensed" id="hotspot-table" >
                                    			<thead>
                                        			<tr>
	                                          			<th></th>
                                          				<th>Tanggal Report</th>
                                          				<th>Est Total Populasi</th>
                                          				<th>Populasi Berpindah</th>
                                          				<th>Populasi Baru</th>
                                          				<th>Populasi Terjangkau</th>
                                          				<th>Jenis Populasi Kunci</th>
                                          				<th>Status</th>
                                          				<th>Action</th>
                                        			</tr>
                                    			</thead>
                                  			</table>
                                		</div>
                              		</div>
                        		</div>
                          	</div>
                      	</div>

                  	</div>
                  	<a href="{{ route('admin.detail.create', ['id'=>$id]) }}" class="btn btn-info">Tambah Laporan</a>
                  	<a href="{{ route('admin.hotspot.index') }}" class="btn btn-danger">Kembali</a>
                </div>
          	</div>
                    
        </div>
  	</div>
</div>

@stop