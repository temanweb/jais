@extends('layouts.admin')

@section('content')

<script type="text/javascript" class="init">
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table class="rowdetails">'+
            '<tr>'+
                '<td>Jenis Kelamin&nbsp; </td>'+
                '<td>:&nbsp;'+d[8]+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Pendidikan&nbsp; </td>'+
                '<td>:&nbsp;'+d[11]+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Phone&nbsp; </td>'+
                '<td>:&nbsp;'+d[9]+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Alamat&nbsp; </td>'+
                '<td>:&nbsp;'+d[10]+'</td>'+
            '</tr>'+

        '</table>';
    }

	$(function() {
    	var table = $('#users-table').DataTable({
    			processing: true,
    			serverSide: true,
            	ajax: '{!! route('admin.users.data') !!}',
	        	columns: [
	            	{ data: '0', name: 'id', orderable: false, searchable: false },
	            	{ data: '1', name: 'name' },
	            	{ data: '2', name: 'email' },
	            	{ data: '3', name: 'tipe' },
	            	{ data: '4', name: 'organisasi' },
	            	{ data: '5', name: 'status' },
	            	{ data: '6', name: 'created_at' },
	            	{ data: '7', name: 'updated_at' },
	            	{ data: '12', name: 'action', orderable: false, searchable: false }
	        	],
            	order: []
    	});
    	table.on( 'order.dt search.dt', function () {
    		table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        		cell.innerHTML = i+1;
    		});
    	}).draw();

    	$('#users-table').on('click', 'button', function () {
    		var tr = $(this).closest('tr');
    		var row = table.row( tr );

    		if ( row.child.isShown() ) {
        		// This row is already open - close it
        		row.child.hide();
        		tr.removeClass('shown');
    		} else {
        		// Open this row
        		row.child( format(row.data()) ).show();
        		tr.addClass('shown');
    		}
    	});

		$('#users-table').DataTable().on('click', '.btn-delete[data-remote]', function (e) {
			$.ajaxSetup({
        		headers: {
            		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        		}
    		});
			
			var token =  $("input[name=_token]").val();
			var x = confirm('Apakah Anda ingin menghapus data ini?');
			if (x) {
		    	e.preventDefault();
		    	var url = $(this).data('remote');
		    	// confirm then
		    	$.ajax({
		        	url: url,
		        	type: 'DELETE',
		        	dataType: 'json',
		        	data: { method: '_DELETE', submit:true }
		    	}).always(function (data) {
		    		$('#users-table').DataTable().draw(false);
		    	});
			} else {
				return false;
			}
		});

	});
</script>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">User Management</h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('flash_message') !!}
                    </div>
                @endif
				<div class="dataTable_wrapper">
					<table class="table table-striped table-hover table-condensed" id="users-table">
						<thead>
							<tr>
								<th></th>
								<th>Nama</th>
								<th>Email</th>
								<th>Tipe</th>
								<th>Organisasi</th>
								<th>Status</th>
								<th>Created</th>
								<th>Updated</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
				<div>
				<a href="{{ route('admin.users.create') }}" class="btn btn-info"> Tambah User</a>
				</div>
			</div>
		</div>
	</div>
</div>

@stop