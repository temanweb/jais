@extends('layouts.admin')

@section('content')
<script type="text/javascript">
$(function() {
    $('#panduan-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('panduan.data') !!}',
        columns: [
            { data: '0', name: 'id', orderable: false, searchable: false },
            { data: '1', name: 'judul' },
            { data: '2', name: 'action', orderable: false, searchable: false }
        ]
    });
});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Panduan</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                  	<table class="table table-striped table-hover table-condensed" id="panduan-table" >
                        <thead>
                    		<tr>
                              	<th></th>
								<th>Judul</th>
                				<th>Action</th>
                    		</tr>
	                    </thead>
                  	</table>
                </div>
                <div><a href="add_hotspot.html" class="btn btn-info">Tambah Hotspot</a></div>

                <!-- Modal -->
                <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
                  	<div class="modal-dialog" role="document">
                        <div class="modal-content">
                          	<div class="modal-header modalhapusheader">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            	<h4 class="modal-title" id="myModalLabel1">Edit Hotspot</h4>
                          	</div>
                          	<div class="modal-body">
                                <form role="form">
                                  	<div class="form-group">
                                    	<label>Nama Hotspot</label>
                                        <input class="form-control" value="Hotel Palem">
                                  	</div>
                            	  	<div class="form-group">
                                        <label>Kota</label>
                                        <select class="form-control">
                                          	<option>Jakarta Barat</option>
                                          	<option>Jakarta Utara</option>
                                          	<option selected>Jakarta Selatan</option>
                                          	<option>Jakarta Timur</option>
                                          	<option>Jakarta Pusat</option>
                                          	<option>Kep Seribu</option>
                                        </select>
                                  	</div>
                            	  	<div class="form-group">
                                        <label>Kecamatan</label>
                                        <select class="form-control">
                                          	<option>Kebayoran Baru</option>
                                          	<option>Kebayoran Lama</option>
                                          	<option>Pesanggrahan</option>
                                          	<option selected>Cilandak</option>
                                          	<option>Pasar Minggu</option>
                                          	<option>Jagakarsa</option>
                                          	<option>Mampang Prapatan</option>
                                          	<option>Pancoran</option>
                                          	<option>Tebet</option>
                                          	<option>Setiabudi</option>
                                        </select>
                                  	</div>
                            	  	<div class="form-group">
                                        <label>Kelurahan</label>
                                        <select class="form-control">
                                          	<option selected>Cipete Selatan</option>
                                          	<option>Gandaria Selatan</option>
                                          	<option>Cilandak Barat</option>
                                          	<option>Lebak Bulus</option>
                                          	<option>Pondok Labu</option>
                                        </select>
                                  	</div>
                                  	<div class="form-group">
                                        <label>Jenis Hotspot</label>
                                        <input class="form-control" value="Hotel">
                                  	</div>
                                  	<div class="text-center">
                                      	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      	<button type="button" class="btn btn-primary" data-dismiss="modal">Simpan</button>
                                  	</div>
                            	</form>
                          	</div>
                        </div>
                  	</div>
                </div>
              	<!--modal-->
              	
              	<!-- Modal -->
              	<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    <div class="modal-dialog modalhapus" role="document">
                      	<div class="modal-content">
                            <div class="modal-header modalhapusheader">
                              	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              	<h4 class="modal-title" id="myModalLabel2">Hapus Hotspot</h4>
                            </div>
                            <div class="modal-body">
                                Apakah anda yakin akan menghapus entri?<br/><br/>
                            </div>
                            <div class="text-center">
                              	<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                              	<button type="button" class="btn btn-primary" data-dismiss="modal">Ya</button><br/><br/>
                            </div>
                      	</div>
                    </div>
              	</div>
                <!--modal-->
            
            </div>
        </div>
        <!-- /.table-responsive -->

    </div>
	<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@stop