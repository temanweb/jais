@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Layanan</h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Tambah Layanan
			</div><!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
            		<div class="col-md-12">
                		@include('partials.alerts.errors')
            		</div>
					<div class="col-lg-6">
						{!! Form::model($data, ['method'=>'PATCH', 'route' => ['admin.layanan.update', $data->id]]) !!}
							<div class="form-group">
								{!! Form::label('nama', 'Nama Layanan') !!}
								{!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('kota', 'Kota') !!}
								{!! Form::select('kota_id', $kota, Input::old('kota_id'), ['class'=>'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('alamat', 'Alamat') !!}
								{!! Form::textarea('address', Input::old('address'), ['class'=>'form-control', 'rows'=>'2']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('jenis', 'Jenis Layanan') !!}
								<div class="row">
									<div class="col-md-6">
										@foreach ($listlayanan as $k=>$v)
										<div class="checkbox">
											<label>{!! Form::checkbox('jenis[]', $k, in_array($v, $data->jenis)) !!}{{ $v }}</label>
										</div>
										@endforeach
									</div>
								</div>
							</div>
							<div class="form-group">
								{!! Form::label('koordinat', 'Koordinat') !!}
								<div class="input-group">
									{!! Form::text('longitude', Input::old('longitude'), ['class'=>'form-control', 'placeholder'=>'Longitude']) !!}
									<span class="input-group-addon"></span>
									{!! Form::text('latitude', Input::old('latitude'), ['class'=>'form-control', 'placeholder'=>'Latitude']) !!}
								</div>
							</div>
							<div class="form-group">
								{!! Form::label('catatan', 'Catatan') !!}
								{!! Form::textarea('description', Input::old('description'), ['class'=>'form-control', 'rows'=>'2']) !!}
							</div>
							<div>
								{!! Form::button('Simpan', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
								<a href="{{ URL::route('admin.layanan.index') }}" class="btn btn-danger">
                    				<i class="icon ti-back-left"></i><span>Kembali</span>
                				</a>
							</div>
						{!! Form::close() !!}
					</div>
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Keterangan
							</div>
							<div class="panel-body">
								<dl>
									<dt>Jenis Layanan</dt>
								</dl>
								<ul>
									<li><strong>HIV : </strong>Testing HIV</li>
									<li><strong>IMS : </strong>Pemeriksaan Infeksi Menular Seksual (IMS)</li>
									<li><strong>PITC : </strong>PITC (Provider-Initiated Testing and Counselling)</li>
									<li><strong>LASS : </strong>Layanan Alat Suntik Steril (LASS)</li>
									<li><strong>PTRM : </strong>Program Terapi Rumatan Metadon (PTRM)</li>
									<li><strong>PMTCT : </strong>Pencegahan penularan HIV dari ibu ke anak (PMTCT)</li>
									<li><strong>VCT : </strong>Konseling dan tes sukarela untuk HIV (VCT)</li>
								</ul>
							</div><!-- /.panel-body -->
						</div>
					</div><!-- /.table-responsive -->
				</div><!-- /.panel-body -->
			</div><!-- /.panel -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
</div><!-- /#page-wrapper -->


@stop