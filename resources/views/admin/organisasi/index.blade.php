@extends('layouts.admin')

@section('content')

<script type="text/javascript" class="init">
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table class="rowdetails">'+
            '<tr>'+
                '<td>Longitude&nbsp; </td>'+
                '<td>:&nbsp;'+d[2]+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Latitude&nbsp; </td>'+
                '<td>:&nbsp;'+d[3]+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Catatan&nbsp; </td>'+
                '<td>:&nbsp;'+d[5]+'</td>'+
            '</tr>'+

        '</table>';
    }

	$(function() {
    	var table = $('#organisasi-table').DataTable({
    			processing: true,
    			serverSide: true,
            	ajax: '{!! route('admin.organisasi.data') !!}',
	        	columns: [
	            	{ data: '0', name: 'id', orderable: false, searchable: false },
	            	{ data: '1', name: 'name' },
	            	{ data: '2', name: 'action', orderable: false, searchable: false }
	        	],
            	order: []
    	});
    	table.on( 'order.dt search.dt', function () {
    		table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        		cell.innerHTML = i+1;
    		});
    	}).draw();

    	/*$('#organisasi-table').on('click', 'button', function () {
    	//$('#layanan-table').DataTable().$('.btn-view').on('click', function (e) {
    		var tr = $(this).closest('tr');
    		var row = table.row( tr );

    		if ( row.child.isShown() ) {
        		// This row is already open - close it
        		row.child.hide();
        		tr.removeClass('shown');
    		} else {
        		// Open this row
        		row.child( format(row.data()) ).show();
        		tr.addClass('shown');
    		}
    	});*/

		$('#organisasi-table').DataTable().on('click', '.btn-delete[data-remote]', function (e) {
			$.ajaxSetup({
        		headers: {
            		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        		}
    		});
			
			var token =  $("input[name=_token]").val();
			var x = confirm('Apakah Anda ingin menghapus data ini?');
			if (x) {
		    	e.preventDefault();
		    	var url = $(this).data('remote');
		    	// confirm then
		    	$.ajax({
		        	url: url,
		        	type: 'DELETE',
		        	dataType: 'json',
		        	data: { method: '_DELETE', submit:true }
		    	}).always(function (data) {
		    		$('#organisasi-table').DataTable().draw(false);
		    	});
			} else {
				return false;
			}
		});

	});
</script>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Organisasi</h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<!-- /.panel-heading -->
			<div class="panel-body">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('flash_message') !!}
                    </div>
                @endif
                <div class="custom-alert"></div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-hover table-condensed" id="organisasi-table">
						<thead>
							<tr>
								<th></th>
								<th>Nama</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
				<div>
					<a href="{{ route('admin.organisasi.create') }}" class="btn btn-info">Tambah Organisasi</a>
				</div>
			</div>
			<!-- /.table-responsive -->

		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


@stop