@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Organisasi - Tambah Organisasi</h1>
	</div>
</div>

<!--main content-->
<div class="main-content">
    <!--theme panel-->
    <div class="panel">
        <div class="panel-body">
            <!--form-heading-->
            <div class="col-md-12">
                @include('partials.alerts.errors')
            </div>
            <!--form-heading-->
            {!! Form::open(['route'=>'admin.organisasi.store']) !!}
                <div class="col-md-12">
                    <!--Default Form-->
                    <div class="form-group">
                        {!! Form::label('name', 'Name:', ['class'=>'control-label']) !!}
                        {!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
                    </div>
                    {!! Form::button('Simpan', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
                    <a href="{{ URL::route('admin.organisasi.index') }}" class="btn btn-danger">
                        <i class="icon ti-back-left"></i>
                        <span>Kembali</span>
                    </a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!--theme panel-->
</div>
<!--main content-->

@stop