@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Organisasi</h1>
	</div>
</div>

<div class="main-content">
    <div class="panel">
        <div class="panel-body">
            <!--form-heading-->
            <div class="col-md-12">
                @include('partials.alerts.errors')
            </div>
            <!--form-heading-->
            {!! Form::model($data, ['method'=>'PATCH', 'route' => ['admin.organisasi.update', $data->id]]) !!}
                <div class="col-md-12">
                    <!--Default Form-->
                    <div class="form-group">
						{!! Form::label('nama', 'Nama Organisasi') !!}
						{!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
                    </div>
                    {!! Form::button('Simpan', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
                    <a href="{{ URL::route('admin.organisasi.index') }}" class="btn btn-danger">
                        <i class="icon ti-back-left"></i>
                        <span>Kembali</span>
                    </a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@stop