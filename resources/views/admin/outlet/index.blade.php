@extends('layouts.admin')

@section('content')
<script type="text/javascript">
$(function() {
    $('#outlet-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('outlet.data') !!}',
        columns: [
            { data: '0', name: 'id' },
            { data: '1', name: 'name' },
            { data: '2', name: 'address' },
            { data: '3', name: 'longitude' },
            { data: '4', name: 'latitude' },
            { data: '5', name: 'kota_id' },
            { data: '6', name: 'action', orderable: false, searchable: false }
        ]
    });
});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Outlet Kondom</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

        	<!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                  	<table class="table table-striped table-hover table-condensed" id="outlet-table" >
                        <thead>
                    		<tr>
                              	<th></th>
                              	<th>Nama Outlet</th>
                              	<th>Alamat</th>
                              	<th>Longitude</th>
                              	<th>Latitude</th>
                              	<th>Kota</th>
                              	<th>Action</th>
                    		</tr>
                        </thead>
                  	</table>
                </div>
            	<div>
                	<a href="add_outlet.html" class="btn btn-info">Tambah Outlet</a>

                	<!-- Modal -->
                    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
                      	<div class="modal-dialog" role="document">
                            <div class="modal-content">
                              	<div class="modal-header modalhapusheader">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel1">Edit Outlet</h4>
                              	</div>
                              	<div class="modal-body">
                                    <form role="form">
                                  		<div class="form-group">
                                        	<label>Nama Outlet</label>
                                        	<input class="form-control" value="Maimu">
                                      	</div>
                                      	<div class="form-group">
                                        	<label>Alamat</label>
                                        	<textarea class="form-control" rows="2" >Jl. Karya Sakti No. 22</textarea>
                                      	</div>
                                      	<div class="form-group">
                                      		<label>Koordinat</label>
                                      		<div class="input-group">
                                        		<input type="text" class="form-control" value="-6.204273"/>
                                        		<span class="input-group-addon"> </span>
                                        		<input type="text" class="form-control" value="106.752951"/>
                                      		</div>
                                    	</div>
                                      	<div class="form-group">
                                    		<label>Catatan</label>
                                        	<textarea class="form-control" rows="2">Buka hari Senin - Jumat, pukul 09:00 - 15:00 WIB</textarea>
                                      	</div>
                                      	<div class="text-center">
                                      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      		<button type="button" class="btn btn-primary" data-dismiss="modal">Simpan</button>
                                    	</div>
                                	</form>
                              	</div>
                            </div>
                      	</div>
                    </div>
                  	<!--modal-->
                  	
                  	<!-- Modal -->
                  	<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    	<div class="modal-dialog modalhapus" role="document">
                          	<div class="modal-content">
                            	<div class="modal-header modalhapusheader">
                                  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  	<h4 class="modal-title" id="myModalLabel2">Hapus Outlet</h4>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin akan menghapus entri?<br/><br/>
                                </div>
                                <div class="text-center">
                                  	<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                  	<button type="button" class="btn btn-primary" data-dismiss="modal">Ya</button><br/><br/>
                                </div>
                          	</div>
                        </div>
                  	</div>
                    <!--modal-->
                            
                </div>
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@stop