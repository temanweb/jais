@extends('layouts.admin')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Report Hotspot Baru</h1>
            	
				@if (isset($typeChart))
            	<div class="panel panel-default">
            		<div class="panel-body">
            			<div class="row">
            				<div class="col-md-12">
            					<div id="container" style="width:100%; height:400px;"></div>
								<script type="text/javascript" src="{{ asset('js/highcharts/js/highcharts.js') }}"></script>
								<script type="text/javascript">
				    				$(function () {
				        				$('#container').highcharts(
				            				{!! json_encode($typeChart) !!}
				        				);
				    				});
								</script>
            				</div>
            			</div>
            		</div>
            	</div>
				@endif

            	@if (isset($table))
            	<div class="panel panel-default">
        			<div class="panel-heading">
          				<dt>Report Hotspot Baru</dt>
          				<dd>Periode Januari - Desember 2016</dd>
        			</div>
        			<div class="panel-body">
        				{!! $table !!}
      				</div>
    			</div>
    			@endif

        	</div>
      	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>

@stop