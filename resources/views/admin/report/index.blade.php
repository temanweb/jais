@extends('layouts.admin')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Report Tahunan</h1>
            	<div class="panel panel-default">
            		<div class="panel-body">
                    	<div class="row">
			                <div class="col-md-12">
			                    @include('partials.alerts.errors')
			                </div>
                    		<div class="col-md-12">
                    			{!! Form::open(['route'=>'admin.report.store', 'class'=>'form-inline', 'role'=>'form']) !!}
                    			<div class="form-group">
                    				{!! Form::label('testing', 'Kota : ') !!}
                    				{!! Form::select('kota_id', $kota, Input::old('kota_id'), ['class'=>'form-control', 'required'=>'required']) !!}
                    			</div>
                    			<div class="form-group">
                    				{!! Form::button('Cari', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
                    			</div>
                    			{!! Form::close() !!}
                    		</div>
                    	</div>
            		</div>
            	</div>

				@if (isset($yearChart))
            	<div class="panel panel-default">
            		<div class="panel-body">
            			<div class="row">
            				<div class="col-md-12">
            					<div id="container" style="width:100%; height:400px;"></div>
								<script type="text/javascript" src="{{ asset('js/highcharts/js/highcharts.js') }}"></script>
								<script type="text/javascript">
				    				$(function () {
				        				$('#container').highcharts(
				            				{!! json_encode($yearChart) !!}
				        				);
				    				});
								</script>
            				</div>
            			</div>
            		</div>
            	</div>
				@endif

            	@if (isset($namakota) && isset($table))
            	<div class="panel panel-default">
        			<div class="panel-heading">
          				<dt>Report Tahunan Kota : {{ $namakota }}</dt>
          				<dd>Periode Januari - Desember 2016</dd>
        			</div>
        			<div class="panel-body">
        				{!! $table !!}
      				</div>
    			</div>
    			@endif

        	</div>
      	</div>
        <!-- /.col-lg-12 -->
	</div>
    <!-- /.row -->
</div>

@stop