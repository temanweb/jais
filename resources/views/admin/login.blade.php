@extends('layouts.login')

@section('content')

    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                @if(Session::has('flash_message'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('flash_message') !!}
                    </div>
                @endif
                @include('partials.alerts.errors')

                {!! Form::open(array('url'=>'admin/login')) !!}
                    <fieldset>
                        <div class="form-group">
                        	{!! Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'E-mail', 'type'=>'email', 'autofocus'=>'autofocus')) !!}
                        </div>
                        <div class="form-group">
                        	{!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password', 'type'=>'password', 'value'=>'')) !!}
                        </div>
                        {!! Form::button('Login', array('class'=>'btn btn-lg btn-success btn-block', 'type'=>'submit', 'name'=>'login', 'value'=>'Login')) !!}
                    </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop