@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Ubah Profil</h1>
	</div>
</div>

<!--main content-->
<div class="main-content">
    <!--theme panel-->
    <div class="panel">
        <div class="panel-body">
            <!--form-heading-->
            <div class="col-md-12">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('flash_message') !!}
                    </div>
                @endif

                @include('partials.alerts.errors')
            </div>
            <!--form-heading-->
            {!! Form::model($data, ['method'=>'PATCH', 'route' => ['admin.profile.update', $data->id]]) !!}
                <div class="col-md-12">
                    <!--Default Form-->
                    <div class="form-group">
                        {!! Form::label('email', 'Email:', ['class'=>'control-label']) !!}
                        {!! Form::text('email', Input::old('email'), ['class'=>'form-control', 'readonly'=>'readonly']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Password:', ['class'=>'control-label']) !!}
                        {!! Form::password('password', ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tipe', 'Tipe User:', ['class'=>'control-label']) !!}
                        {!! Form::select('user_tipe', $tipe, Input::old('user_tipe'), ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('nama', 'Nama Lengkap:', ['class'=>'control-label']) !!}
                        {!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('jenis_kelamin', 'Jenis Kelamin', ['class'=>'control-label']) !!}
                        {!! Form::select('jenis_kelamin', $jk, Input::old('jenis_kelamin'), ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('pendidikan', 'Pendidikan:', ['class'=>'control-label']) !!}
                        {!! Form::select('pendidikan', $didik, Input::old('pendidikan'), ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('no_hp', 'No. HP:', ['class'=>'control-label']) !!}
                        {!! Form::text('phone', Input::old('phone'), ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('alamat', 'Alamat:', ['class'=>'control-label']) !!}
                        {!! Form::textarea('alamat', Input::old('alamat'), ['class'=>'form-control', 'rows'=>'2']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('organisasi', 'Organisasi', ['class'=>'control-label']) !!}
                        {!! Form::select('organization_id', $organisasi, Input::old('organization_id'), ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('status', 'Status:', ['class'=>'control-label']) !!}
                        {!! Form::select('status', array(''=>'Pilih', '1'=>'Aktif', '2'=>'Tidak Aktif'), Input::old('status'), ['class'=>'form-control']) !!}
                    </div>
                    {!! Form::button('Simpan', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!--theme panel-->
</div>
<!--main content-->

@stop