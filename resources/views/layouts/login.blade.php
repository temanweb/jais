<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	
	<!--320-->
	<title>Login - Jakarta AIDS</title>
	<!-- <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon"> -->
	<!-- Bootstrap CSS -->

	{!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
	{!! Html::style('bower_components/metisMenu/dist/metisMenu.min.css') !!}
	{!! Html::style('dist/css/sb-admin-2.css') !!}
	{!! Html::style('bower_components/font-awesome/css/font-awesome.min.css') !!}

</head>

<body>

	<div class="signin_wrapper">
		<div class="row">
			<div class="right_block">
				<div class="row">
					@yield('content')
				</div>                
			</div>
			<!-- right_block -->
		</div>
		<!-- row -->


	</div>
	<!-- wrapper -->

	<!-- jQuery -->
	{!! Html::script('bower_components/jquery/dist/jquery.js') !!}
	{!! Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
	{!! Html::script('bower_components/metisMenu/dist/metisMenu.min.js') !!}
	{!! Html::script('dist/js/sb-admin-2.js') !!}

</body>
</html>