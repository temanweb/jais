<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JAKARTA AIDS - DASHBOARD</title>

    <!-- Bootstrap Core CSS -->
    {!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}

    <!-- MetisMenu CSS -->
    {!! Html::style('bower_components/metisMenu/dist/metisMenu.min.css') !!}

    <!-- DataTables CSS -->
    {!! Html::style('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') !!}

    <!-- DataTables Responsive CSS -->
    {!! Html::style('bower_components/datatables-responsive/css/dataTables.responsive.css') !!}

    <!-- jQuery -->
    <!-- Custom CSS -->
    {!! Html::style('dist/css/sb-admin-2.css') !!}

    <!-- Custom Fonts -->
    {!! Html::style('bower_components/font-awesome/css/font-awesome.min.css') !!}

    <!-- jQuery -->
    {!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}

    {!! Html::script('http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js') !!}
    {!! Html::script('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') !!}

    <!-- Bootstrap Core JavaScript -->
    {!! Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">JAKARTA AIDS - DASHBOARD</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ route('admin.profile.index') }}"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
                        <li><a href="{{ route('admin.layanan.index') }}"><i class="fa fa-medkit fa-fw"></i> Layanan</a></li>
                        <li><a href="{{ route('admin.hotspot.index') }}"><i class="fa fa-bullseye fa-fw"></i> Hotspot</a></li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Report<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="{{ route('admin.report.index') }}">Tahunan</a></li>
                                <li><a href="{{ route('admin.report.monthly') }}">Bulanan</a></li>
                                <li><a href="{{ route('admin.report.active') }}">Hotspot Aktif</a></li>
                                <li><a href="{{ route('admin.report.nonactive') }}">Hotspot Tidak Aktif</a></li>
                                <li><a href="{{ route('admin.report.hotspotnew') }}">Hotspot Baru</a></li>
                                <li><a href="{{ route('admin.report.estimasi') }}">Perkiraan Populasi</a></li>
                                <li><a href="{{ route('admin.report.populasi') }}">Populasi Terjangkau</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('admin.organisasi.index') }}"><i class="fa fa-building fa-fw"></i> Organisasi</a></li>
                        <li><a href="{{ route('admin.users.index') }}"><i class="fa fa-user fa-fw"></i> User Management</a></li>
                    </ul>

                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            @yield('content')
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- Metis Menu Plugin JavaScript -->
    {!! Html::script('bower_components/metisMenu/dist/metisMenu.min.js') !!}

    <!-- Morris Charts JavaScript -->
    {!! Html::script('bower_components/raphael/raphael-min.js') !!}

    <!-- Custom Theme JavaScript -->
    {!! Html::script('dist/js/sb-admin-2.js') !!}



</body>

</html>