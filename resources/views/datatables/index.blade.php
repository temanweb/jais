@extends('layouts.admin')

@section('content')
<script type="text/javascript">
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('hotspot.data') !!}',
        columns: [
            { data: '0', name: 'id' },
            { data: '1', name: 'name' },
            { data: '2', name: 'email' },
            { data: '3', name: 'created_at' },
            { data: '4', name: 'updated_at' }
        ]
    });
});
</script>


    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
    </table>

@stop