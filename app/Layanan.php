<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    //
    protected $table = "lokasi_layanan";

    protected $fillable = [
    	'id',
    	'name',
    	'latitude',
    	'longitude',
    	'address',
    	'description',
    	'kota_id'
    ];
}
