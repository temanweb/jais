<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotspot extends Model
{
    //
    protected $table = "hotspot";

    protected $fillable = [
    	'name',
    	'kota_id',
    	'kecamatan_id',
    	'kelurahan_id',
    	'tipe',
    	'user_id',
    	'created_at',
    	'updated_at'
    ];
}
