<?php
/**
 * Created by PhpStorm.
 * User: denipermana
 * Date: 6/2/16
 * Time: 9:31 PM
 */

namespace App\Library;


use GuzzleHttp\Client;
use Cache;
use Session;
class ApiClient
{
    const CLIENT_ID = 1;
    const CLIENT_SECRET = '226655d5e7bf98a0d31602557d23f194';
    const GRANT_TYPE = "password";
    private $client;
    private $username;
    private $password;
    private $user;
    private $token;
    private $refreshToken;
    private $tokenType;
    private $expires;

    private static $apiClient = null;
    /**
     * ApiClient constructor.
     */
    public function __construct($username, $password)
    {
        $this->client = new Client(['base_uri' => config('app.api_url')]);
        $this->username = $username;
        $this->password = $password;
    }

    public static function instantiate($username, $password)
    {
        if(self::$apiClient == null){

            self::$apiClient = new ApiClient($username, $password);
//            if(self::$apiClient->checkToken()){
                self::$apiClient->login();
//            }
            return self::$apiClient;

        }
        return self::$apiClient;
    }
    private function login()
    {
        $token = $this->generateToken();
        if($token->getStatusCode() == 200){
            $this->saveSession(json_decode($token->getBody()));
        }

    }
    private function generateToken()
    {
        $params = ['form_params' =>
            [
                'client_id' => self::CLIENT_ID,
                'client_secret' => self::CLIENT_SECRET,
                'grant_type' => self::GRANT_TYPE,
                'username' => $this->username,
                'password' => $this->password
            ]
        ];
        try{
            $request = $this->client->request("POST", "oauth/access_token", $params);
            return $request;
        }catch (\Exception $e){
            $responseBody = $e->getResponse();
            return $responseBody;
        }
    }

    private function saveSession($result)
    {
        $this->setCacheToken($result);
    }
    public function setUser($user)
    {
        $this->user = $user;
    }
    public function getUser()
    {
        return $this->user;
    }

    private function getTokenObject()
    {
        if(Session::has('token')){
            return Session::get('token');
        }
        return null;
    }
    private function setCacheToken($result)
    {

        if(!Session::has('token')){
            Session::put('token', $result);
        }

        return;
    }
}