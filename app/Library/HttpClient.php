<?php
/**
 * Created by PhpStorm.
 * User: denipermana
 * Date: 6/2/16
 * Time: 11:00 PM
 */

namespace App\Library;


use App\Http\Requests\Request;
use GuzzleHttp\Client;
use Cache;
use Session;

class HttpClient
{
    private $token;
    private $client;
    const URL_PREFIX = "api";
    /**
     * HttpClient constructor.
     */
    public function __construct()
    {
        $this->token = Session::get('token');
        $this->client = new Client(['base_uri' => config('app.api_url')]);
    }


    public function get($url, array $param = [], $debug = false)
    {
        $param = [
            "query" => $param,
            'debug' => $debug,
            "headers" => [
                "Authorization" => $this->token->token_type . " " . $this->token->access_token
            ]
        ];

        try {
            $result = $this->client->request("GET", 'api/'. $url, $param);
            return $result;
        }catch (\Exception $e){
            $responseBody = $e->getResponse();
            if($responseBody->getStatusCode() == 401){
                $this->refreshToken();
            }
            return $responseBody;
        }
    }

    public function post($url, array $params, $debug = false)
    {
        $param = [
            "form_params" => $params,
            'debug' => $debug,
            "headers" => [
                "Authorization" => $this->token->token_type . " " . $this->token->access_token
            ]
        ];
        try {
            $result = $this->client->request("POST", 'api/'. $url, $param);
            return $result;
        }catch (\Exception $e){
            $responseBody = $e->getResponse();
            if($responseBody->getStatusCode() == 401){
                $this->refreshToken();
            }
            return $responseBody;
        }
    }

    public function put($url, array $params, $debug = false)
    {
        $param = [
            "form_params" => $params,
            'debug' => $debug,
            "headers" => [
                "Authorization" => $this->token->token_type . " " . $this->token->access_token
            ]
        ];
        try {
            $result = $this->client->request("PUT", 'api/'. $url, $param);
            return $result;
        }catch (\Exception $e){
            $responseBody = $e->getResponse();
            if($responseBody->getStatusCode() == 401){
                $this->refreshToken();
            }
            return $responseBody;
        }
    }
    public function delete($url, array $param = [], $debug = false)
    {
        $param = [
            "form_params" => $param,
            'debug' => $debug,
            "headers" => [
                "Authorization" => $this->token->token_type . " " . $this->token->access_token
            ]
        ];
        try {
            $result = $this->client->request("DELETE", 'api/'. $url, $param);
            return $result;
        }catch (\Exception $e){
            $responseBody = $e->getResponse();
            if($responseBody->getStatusCode() == 401){
                $this->refreshToken();
            }
            return $responseBody;
        }
    }

    private function refreshToken()
    {
        $params = ['form_params' =>
            [
                'client_id' => 1,
                'client_secret' => '226655d5e7bf98a0d31602557d23f194',
                'grant_type' => 'refresh_token',
                'refresh_token' => $this->token->refresh_token,
                "headers" => [
                    "Authorization" => $this->token->token_type . " " . $this->token->access_token
                ]
            ]
        ];

        try{
            $result = $this->client->request("POST", "oauth/access_token", $params);
        }catch (\Exception $e){
            $responseBody = $e->getResponse();
            if($responseBody->getStatusCode() == 401){
                Session::flush();
                throw new \Exception("token expired please login again");
            }
            return $responseBody;
        }

    }
}