<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('admin/login', function() {
//	return view('admin.login');
//});
Route::get('/', function() {
	return view('admin.login');
});

Route::get('admin/login', ['as'=>'login', 'uses'=>'Admin\LoginController@index']);
Route::post('admin/login', ['uses'=>'Admin\LoginController@doLogin']);

Route::post('oauth/access_token', function() {
	return Response::json(Authorizer::issueAccessToken());
});

Route::get('api', ['before' => 'oauth', function() {
 	// return the protected resource
 	//echo “success authentication”;
 	$user_id = Authorizer::getResourceOwnerId(); // the token user_id
 	
 	//$user = \App\User::find($user_id);// get the user data from database
	//return Response::json($user);
}]);



Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware' => 'auth'], function() {
	Route::get('/', ['uses'=>'LoginController@index']);
	Route::get('logout', ['as'=>'logout', 'uses'=>'LoginController@logout']);
	Route::get('profile', ['as'=>'admin.profile.index', 'uses'=>'ProfileController@index']);

	Route::get('layanan/data', ['as'=>'admin.layanan.data', 'uses'=>'LayananController@getData']);
	Route::get('hotspot/data', ['as'=>'admin.hotspot.data', 'uses'=>'HotspotController@getData']);
	Route::get('detail/data', ['as'=>'admin.detail.data', 'uses'=>'DetailController@getData']);
	Route::get('users/data', ['as'=>'admin.users.data', 'uses'=>'UsersController@getData']);
	Route::get('organisasi/data', ['as'=>'admin.organisasi.data', 'uses'=>'OrganisasiController@getData']);

	Route::get('dashboard', ['as'=>'dashboard', 'uses'=>'DashboardController@index']);
	Route::get('report/monthly', ['as'=>'admin.report.monthly', 'uses'=>'ReportController@monthly']);
	Route::get('report/active', ['as'=>'admin.report.active', 'uses'=>'ReportController@active']);
	Route::get('report/nonactive', ['as'=>'admin.report.nonactive', 'uses'=>'ReportController@nonactive']);
	Route::get('report/hotspotnew', ['as'=>'admin.report.hotspotnew', 'uses'=>'ReportController@hotspotnew']);
	Route::get('report/populasi', ['as'=>'admin.report.populasi', 'uses'=>'ReportController@populasi']);
	Route::get('report/estimasi', ['as'=>'admin.report.estimasi', 'uses'=>'ReportController@estimasi']);



	Route::post('hotspot/getkecamatan', ['as'=>'admin.hotspot.getkecamatan', 'uses'=>'HotspotController@getkecamatan']);
	Route::post('hotspot/getkelurahan', ['as'=>'admin.hotspot.getkelurahan', 'uses'=>'HotspotController@getkelurahan']);
	Route::post('layanan/destroy', ['as'=>'admin.layanan.destroy', 'uses'=>'LayananController@destroy']);
	Route::post('report/chartmonthly', ['as'=>'admin.report.chartmonthly', 'uses'=>'ReportController@chartmonthly']);

	Route::resource('layanan', 'LayananController');
	Route::resource('hotspot', 'HotspotController');
	Route::resource('detail', 'DetailController');
	Route::resource('users', 'UsersController');
	Route::resource('organisasi', 'OrganisasiController');
	Route::resource('report', 'ReportController');
	Route::resource('profile', 'ProfileController');
});

