<?php

namespace App\Http\Controllers\Admin;

use App\Library\ApiClient;
use App\Library\HttpClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Datatables;
use Illuminate\Support\Collection;

use App\Hotspot;
use App\Classes\Common;
use Carbon\Carbon;

use URL, Input, Redirect, Validator, Session;

class HotspotController extends Controller
{
    private $url_token = 'http://apps.jakartaaids.com/oauth/access_token';
    private $hotspot = 'http://apps.jakartaaids.com/api/hotspot';
    private $filter = 'http://apps.jakartaaids.com/api/filters';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('admin.hotspot.index');
    }


    public function getData() 
    {
        try {
            $req = new HttpClient();
            $hotspot = $req->get("hotspot");
            $output = json_decode($hotspot->getBody(), TRUE);

            $hasil = new Collection();

            foreach ($output as $k=>$v) {
                $hasil->push([
                    'id' => $v['id'],
                    'name' => $v['name'],
                    'kota_id' => $v['kota']['name'],
                    'kecamatan_id' => $v['kecamatan']['name'],
                    'kelurahan_id' => (isset($v['kelurahan'])) ? $v['kelurahan']['name'] : '',
                    'tipe' => $v['tipe_hotspot']['name'],
                    'created_at' => $v['created_at'],
                    'updated_at' => $v['updated_at'],
                    'action' => '
                        <a href="'.URL::to('admin/detail').'/'.$v['id'].'" class="btn btn-success btn-xs" title="Detail"><i class="fa fa-plus-square"></i></a>
                        <a href="'.URL::to('admin/hotspot').'/'.$v['id'].'/edit" class="btn btn-primary btn-xs" title="Edit"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs btn-delete" data-remote="/admin/hotspot/'.$v['id'].'" title="Hapus"><i class="fa fa-times"></i></a>
                    '
                ]);
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return Datatables::collection($hasil)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //$token = $this->token();        
        try {
            $req = new HttpClient();
            $hasil = $req->get("filters", ['type' => 'kota']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $kota[''] = 'Pilih';
            foreach ($output as $k=>$v) {
                $kota[$v['id']] = $v['name'];
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        $libClass = new Common();
        $tipe = $libClass->tipeHotspot();

        return view('admin.hotspot.create', compact('kota', 'tipe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                $req = new HttpClient();
                $hasil = $req->post("hotspot", [
                    'name' => $request->name,
                    'kota_id' => $request->kota_id,
                    'kecamatan_id' => $request->kecamatan_id,
                    'kelurahan_id' => $request->kelurahan_id,
                    'tipe' => $request->tipe
                ]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/hotspot');
                }

                $kota = array();
                $kota[''] = 'Pilih';
                foreach ($output as $k=>$v) {
                    $kota[$v['id']] = $v['name'];
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi. a');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. b');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. c');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. d');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //return view('')
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $req = new HttpClient();
            $hasil = $req->get("filters", ['type' => 'kota']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $kota[''] = 'Pilih';
            foreach ($output as $k=>$v) {
                $kota[$v['id']] = $v['name'];
            }

            $newreq = new HttpClient();
            $newhsl = $newreq->get("hotspot".'/'.$id.'/edit');

            $newout = json_decode($newhsl->getBody(), TRUE);

            if (!empty($newout)) {
                $data = (object) array(
                    'id' => $newout['id'],
                    'name' => $newout['name'],
                    'kota_id' => $newout['kota_id'],
                    'kecamatan_id' => $newout['kecamatan_id'],
                    'kelurahan_id' => $newout['kelurahan_id'],
                    'tipe' => $newout['tipe']
                );
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        $libClass = new Common();
        $tipe = $libClass->tipeHotspot();


        return view('admin.hotspot.edit', compact('kota', 'data', 'tipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules());
        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                $req = new HttpClient();
                $hasil = $req->put("hotspot".'/'.$id, [
                    'name' => $request->name,
                    'kota_id' => $request->kota_id,
                    'kecamatan_id' => $request->kecamatan_id,
                    'kelurahan_id' => $request->kelurahan_id,
                    'tipe' => $request->tipe
                ]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/hotspot');
                }

                $kota = array();
                $kota[''] = 'Pilih';
                foreach ($output as $k=>$v) {
                    $kota[$v['id']] = $v['name'];
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi. a');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. b');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. c');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. d');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $req = new HttpClient();
            $hasil = $req->delete("hotspot".'/'.$id);

            $output = json_decode($hasil->getBody(), TRUE);
            if (isset($output['success']) && !empty($output['success'])) {
                echo json_decode($output['success']);
                exit();
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi. a');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. b');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. c');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. d');
        }
    }

    public function rules($id=0, $merge=[]) {
        return array_merge(
            [
                'name'      => 'required',
                'kota_id'      => 'required',
                'kecamatan_id' => 'required',
                'kelurahan_id' => 'required',
                'tipe'     => 'required'
            ]
        ,$merge);
    }

    public function getkecamatan(Request $request) {
        
        try {
            $req = new HttpClient();
            $hasil = $req->get("filters", ['type' => 'kecamatan', 'kota_id' => $request->kota_id]);

            $output = json_decode($hasil->getBody(), TRUE);

            $hasil = '<select name="kecamatan_id" id="idKecamatan" class="form-control">';
            $hasil .= '<option value="">Pilih</option>';
            if (!empty($output)) {
                foreach ($output as $row) {
                    $selected = (isset($request->kecamatan_id) && !empty($request->kecamatan_id) && $request->kecamatan_id == $row['id']) ? ' selected="selected"' : '';
                    $hasil .= '<option value="'.$row['id'].'"'.$selected.'>'.$row['name'].'</option>';
                }
            }
            $hasil .= '</select>';

            echo json_encode($hasil);
            exit();

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }
    }

    public function getkelurahan(Request $request) {
        
        try {
            $req = new HttpClient();
            $hasil = $req->get("filters", ['type' => 'kelurahan', 'kecamatan_id' => $request->kecamatan_id]);

            $output = json_decode($hasil->getBody(), TRUE);

            $hasil = '<select name="kelurahan_id" id="idKelurahan" class="form-control">';
            $hasil .= '<option value="">Pilih</option>';
            if (!empty($output)) {
                foreach ($output as $row) {
                    $selected = (isset($request->kelurahan_id) && !empty($request->kelurahan_id) && $request->kelurahan_id == $row['id']) ? ' selected="selected"' : '';
                    $hasil .= '<option value="'.$row['id'].'"'.$selected.'>'.$row['name'].'</option>';
                }
            }
            $hasil .= '</select>';

            echo json_encode($hasil);
            exit();

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }
    }

}