<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

use Input, Validator, Session, Redirect, Auth, Config, Route;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::check()){
            return redirect()->to('admin/dashboard');
        }
        return view('admin.login');
    }

    public function doLogin(Request $request) {

        $rules = array(
            'email' => 'required|email',
            'password' => 'required|min:6'
        );
//
        $validator = Validator::make(Input::all(), $rules);
//
        if ($validator->fails()) {
            return Redirect::to('admin/login')
            ->withErrors($validator)
            ->withInput(Input::except('password'));
        } else {
            $attempt = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
          //  dd($attempt);
          
            if($attempt){
                $user = Auth::user();
                return Redirect::to('admin/dashboard');
            }

            return Redirect::to('admin/login')
                ->withErrors('Email atau password salah')
                ->withInput(Input::except('password'));

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout() {
//        \Auth::logout();
        Session::flush();
        return Redirect::to('admin/login')->with('flash_message', 'You are successfully logged out.');
    }
}
