<?php

namespace App\Http\Controllers\Admin;

use App\Library\ApiClient;
use App\Library\HttpClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Datatables;
use Illuminate\Support\Collection;

use App\Hotspot;
use App\Laporanhotspot;
use Carbon\Carbon;

use URL, Input, Redirect, Validator, Session;

class DetailController extends Controller
{
    private $data;
    private $url_token = 'http://apps.jakartaaids.com/oauth/access_token';
    private $url_hotspot = 'http://apps.jakartaaids.com/api/hotspot';
    private $report = 'http://apps.jakartaaids.com/api/hotspot-report';
    
    public function setOutputData($output) {
        $this->data = $output;
    }

    public function getOutputData() {
        return $this->data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $id = $request->id;
        return view('admin.detail.create', compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules());
        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                $req = new HttpClient();
                $hasil = $req->post("hotspot-report", [
                    'id' => $request->id,
                    'estimasi_jumlah_populasi' => $request->estimasi_jumlah_populasi,
                    'estimasi_jumlah_populasi_baru' => $request->estimasi_jumlah_populasi_baru,
                    'estimasi_populasi_berpindah' => $request->estimasi_populasi_berpindah,
                    'populasi_terjangkau' => $request->populasi_terjangkau,
                    'status' => $request->status,
                    //'jenis' => $request->jenis,
                    'notes' => $request->notes
                ]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/detail/'.$request->id);
                }

                $kota = array();
                $kota[''] = 'Pilih';
                foreach ($output as $k=>$v) {
                    $kota[$v['id']] = $v['name'];
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi. a');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. b');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. c');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. d');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $pandu = new HttpClient();
            $hotspot = $pandu->get("hotspot".'/'.$id);

            $output = json_decode($hotspot->getBody(), TRUE);
            $this->setOutputData($output);

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }
        return view('admin.detail.show', compact('id', 'output'));
    }

    public function getData(Request $request) 
    {

        try {
            $pandu = new HttpClient();
            $hotspot = $pandu->get("hotspot".'/'.$request->id);

            $output = json_decode($hotspot->getBody(), TRUE);

            $hasil = new Collection();

            $i = 1;
            foreach ($output['reports'] as $k=>$v) {
                $status = ($v['status'] == '1') ? 'Aktif' : 'Tidak Aktif';
                $hasil->push([
                    'id' => $i,
                    'updated_at' => $v['updated_at'],
                    'estimasi_populasi' => $v['estimasi_populasi'],
                    'populasi_berpindah' => $v['populasi_berpindah'],
                    'populasi_baru' => $v['populasi_baru'],
                    'populasi_terjangkau' => $v['populasi_terjangkau'],
                    'hotspot_id' => $v['hotspot_id'],
                    'status' => $status,
                    'notes' => $v['notes'],
                    'action' => '
                        <button class="btn btn-success btn-xs" title="view"><i class="fa fa-plus-square"></i></button>
                        <a class="btn btn-danger btn-xs btn-delete" data-remote="/admin/detail/'.$v['id'].'" title="Hapus"><i class="fa fa-times"></i></a>
                    '
                ]);
                $i++;
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return Datatables::collection($hasil)->make();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $req = new HttpClient();
            $hasil = $req->delete("hotspot-report".'/'.$id);

            $output = json_decode($hasil->getBody(), TRUE);
            if (isset($output['success']) && !empty($output['success'])) {
                echo json_decode($output['success']);
                exit();
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi. a');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. b');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. c');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi. d');
        }
    }

    public function rules($id=0, $merge=[]) {
        return array_merge(
            [
                'estimasi_jumlah_populasi'              => 'required',
                'estimasi_jumlah_populasi_baru'         => 'required',
                'estimasi_populasi_berpindah'           => 'required',
                'populasi_terjangkau'                   => 'required',
                'status'                                => 'required',
                'jenis'                                 => 'required',
                'notes'                                 => 'required'
            ],
        $merge);
    }

}
