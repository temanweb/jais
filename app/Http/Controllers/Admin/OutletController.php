<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Datatables;
use Illuminate\Support\Collection;

class OutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.outlet.index');
    }

    public function anyData() {
        $client = new Client();
        $res = $client->request('POST', 'http://apps.jakartaaids.com/oauth/access_token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '1',
                'client_secret' => '226655d5e7bf98a0d31602557d23f194',
                'username' => 'deni@duniakreatif.com',
                'password' => 'password'
            ]
        ]);
        $token = json_decode($res->getBody(), TRUE);
        
        $out = new Client();
        $outlet = $out->request('GET', 'http://apps.jakartaaids.com/api/outlet-kondom');
        $output = json_decode($outlet->getBody(), TRUE);

        $users = new Collection();

        foreach ($output as $k=>$v) {
            if (!empty($v['coordinate'])) {
                $koordinat = explode(',', $v['coordinate']);
                $long = $koordinat[0];
                $lat = $koordinat[1];
            } else {
                $long = '';
                $lat = '';
            }

            $users->push([
                'id' => $v['id'],
                'name' => $v['name'],
                'address' => $v['address'],
                'longitude' => $long,
                'latitude' => $lat,
                'kota_id' => $v['kota_id'],
                'action' => '
                    <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit" title="edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#hapus" title="delete"><i class="fa fa-times"></i></a>
                    <button class="btn btn-success btn-xs" title="view"><i class="fa fa-plus-square"></i></button>
                '
            ]);
        }

        return Datatables::collection($users)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
