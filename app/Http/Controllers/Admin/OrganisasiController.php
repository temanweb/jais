<?php

namespace App\Http\Controllers\Admin;

use App\Library\ApiClient;
use App\Library\HttpClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Datatables;
use Illuminate\Support\Collection;

use Carbon\Carbon;
use URL, Input, Redirect, Validator, Session;

class OrganisasiController extends Controller
{
    private $url_token = 'http://apps.jakartaaids.com/oauth/access_token';
    private $organisasi = 'http://apps.jakartaaids.com/api/organization';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.organisasi.index');
    }

    public function getData()
    {
        try {
            $pandu = new HttpClient();
            $hotspot = $pandu->get("organization");

            $output = json_decode($hotspot->getBody(), TRUE);

            $hasil = new Collection();

            foreach ($output as $k=>$v) {
                $hasil->push([
                    'id' => $v['id'],
                    'name' => $v['name'],
                    'action' => '
                        <a href="'.URL::to('admin/organisasi').'/'.$v['id'].'/edit" class="btn btn-primary btn-xs" title="edit"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs btn-delete" data-remote="/admin/organisasi/'.$v['id'].'" title="Hapus"><i class="fa fa-times"></i></a>
                    '
                ]);
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return Datatables::collection($hasil)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.organisasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());
        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                $req = new HttpClient();
                $hasil = $req->post("organization", ['name' => $request->name]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/organisasi');
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$token = $this->token();
        
        try {
            $newreq = new HttpClient();
            $newhsl = $newreq->get("organization".'/'.$id);

            $newout = json_decode($newhsl->getBody(), TRUE);

            if (!empty($newout)) {
                $data = (object) array(
                    'id' => $newout['id'],
                    'name' => $newout['name']
                );
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.organisasi.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());
        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                $req = new HttpClient();
                $hasil = $req->put("organization".'/'.$id, ['name' => $request->name]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/organisasi');
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $req = new HttpClient();
            $hasil = $req->delete("organization".'/'.$id);

            $output = json_decode($hasil->getBody(), TRUE);
            if (isset($output['success']) && !empty($output['success'])) {
                echo json_decode($output['success']);
                exit();
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }
    }

    public function rules($id=0, $merge=[]) {
        return array_merge(
            [
                'name'          => 'required'
            ],
        $merge);
    }

}
