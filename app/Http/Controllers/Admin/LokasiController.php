<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Datatables;
use Illuminate\Support\Collection;

class LokasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.lokasi.index');
    }

    public function anyData() {
        $client = new Client();
        $res = $client->request('POST', 'http://apps.jakartaaids.com/oauth/access_token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '1',
                'client_secret' => '226655d5e7bf98a0d31602557d23f194',
                'username' => 'deni@duniakreatif.com',
                'password' => 'password'
            ]
        ]);
        $token = json_decode($res->getBody(), TRUE);
        
        $url = 'http://apps.jakartaaids.com/api/filters';
        $params = '?type=kecamatan&kota_id=1';
        
        $pandu = new Client();
        $panduan = $pandu->request('GET', $url.$params);
        $output = json_decode($panduan->getBody(), TRUE);
        //$res=$panduan->json();  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
