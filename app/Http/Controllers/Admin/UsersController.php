<?php

namespace App\Http\Controllers\Admin;

use App\Library\ApiClient;
use App\Library\HttpClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Datatables;
use Illuminate\Support\Collection;

use Carbon\Carbon;

use URL, Hash, Validator, Input, Redirect, Session;

class UsersController extends Controller
{
    private $url_token = 'http://apps.jakartaaids.com/oauth/access_token';
    private $user = 'http://apps.jakartaaids.com/api/users';
    private $organisasi = 'http://apps.jakartaaids.com/api/organization';

    private $status = array(''=>'Pilih', '1'=>'Aktif', '2'=>'Tidak Aktif');
    private $jk = array(''=>'Pilih', '1'=>'Pria', '2'=>'Wanita');
    private $didik = array(''=>'Pilih', '1'=>'SMA', '2'=>'S1', '3'=>'S2', '4'=>'S3');
    private $tipe = array(''=>'Pilih', '1'=>'Administrator', '2'=>'Pemantau');

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.users.index');
    }

    public function getData() 
    {
        try {
            $req = new HttpClient();
            $hasil = $req->get("users");

            $output = json_decode($hasil->getBody(), TRUE);

            $data = new Collection();

            foreach ($output as $k=>$v) {
                $data->push([
                    'id' => $v['id'],
                    'name' => $v['name'],
                    'email' => $v['email'],
                    'tipe' => $this->tipe[$v['details']['user_tipe']],
                    'organisasi' => $v['details']['organization']['name'],
                    'status' => $this->status[$v['details']['status']],
                    'created_at' => $v['created_at'],
                    'updated_at' => $v['updated_at'],
                    'jenis_kelamin' => $this->jk[$v['details']['jenis_kelamin']],
                    'hp' => $v['details']['phone'],
                    'alamat' => $v['details']['alamat'],
                    'pendidikan' => $this->didik[$v['details']['pendidikan']],
                    'action' => '
                        <button class="btn btn-success btn-xs btn-view" title="view"><i class="fa fa-plus-square"></i></button>
                        <a href="'.URL::to('admin/users').'/'.$v['id'].'/edit" class="btn btn-primary btn-xs" title="Edit"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs btn-delete" data-remote="/admin/users/'.$v['id'].'" title="Hapus"><i class="fa fa-times"></i></a>
                    '
                ]);
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return Datatables::collection($data)->make();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        try {
            $res = new HttpClient();
            $org = $res->get("organization");

            $output = json_decode($org->getBody(), TRUE);

            $organisasi = array();
            $organisasi[''] = 'Pilih';
            foreach ($output as $k=>$v) {
                $organisasi[$v['id']] = $v['name'];
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        $jk = $this->jk;
        $didik = $this->didik;
        $status = $this->status;
        $tipe = $this->tipe;

        return view('admin.users.create', compact('jk', 'didik', 'status', 'tipe', 'organisasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                $pandu = new HttpClient();
                $panduan = $pandu->post("users", [
                    'email' => Input::get('email'),
                    'password' => Input::get('password'),
                    'user_tipe' => Input::get('tipe'),
                    'name' => Input::get('name'),
                    'jenis_kelamin' => Input::get('jenis_kelamin'),
                    'pendidikan' => Input::get('pendidikan'),
                    'phone' => Input::get('phone'),
                    'alamat' => Input::get('alamat'),
                    'organization' => Input::get('organization_id'),
                    'status' => Input::get('status')
                ]);

                $output = json_decode($panduan->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/users');
                } else {
                    return Redirect::back();
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.')->withInput(Input::all());
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.')->withInput(Input::all());
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.')->withInput(Input::all());
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.')->withInput(Input::all());
            } catch (\Exception $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.')->withInput(Input::all());
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {
            $org = new HttpClient();
            $hslorg = $org->get("organization");
            $neworg = json_decode($hslorg->getBody(), TRUE);

            $organisasi = array();
            $organisasi[''] = 'Pilih';
            foreach ($neworg as $k=>$v) {
                $organisasi[$v['id']] = $v['name'];
            }

            $req = new HttpClient();
            $hasil = $req->get("users".'/'.$id);

            $output = json_decode($hasil->getBody(), TRUE);

            $data = (object) array(
                'id' => $output['id'],
                'name' => $output['name'],
                'email' => $output['email'],
                'jenis_kelamin' => (isset($output['details']['jenis_kelamin'])) ? $output['details']['jenis_kelamin'] : '',
                'pendidikan' => (isset($output['details']['pendidikan'])) ? $output['details']['pendidikan'] : '',
                'alamat' => (isset($output['details']['alamat'])) ? $output['details']['alamat'] : '',
                'phone' => (isset($output['details']['phone'])) ? $output['details']['phone'] : '',
                'organization_id' => (isset($output['details']['organization_id'])) ? $output['details']['organization_id'] : '',
                'user_tipe' => (isset($output['details']['user_tipe'])) ? $output['details']['user_tipe'] : '',
                'status' => (isset($output['details']['status'])) ? $output['details']['status'] : ''
            );

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        $tipe = $this->tipe;
        $jk = $this->jk;
        $didik = $this->didik;

        return view('admin.users.edit', compact('data', 'organisasi', 'tipe', 'jk', 'didik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                $req = new HttpClient();
                $hasil = $req->put("users".'/'.$id, [
                    'email' => $request->email,
                    'password' => $request->password,
                    'user_tipe' => $request->tipe,
                    'name' => $request->name,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'pendidikan' => $request->pendidikan,
                    'phone' => $request->phone,
                    'alamat' => $request->alamat,
                    'organization' => $request->organization_id,
                    'status' => $request->status
                ]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/users');
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $req = new HttpClient();
            $hasil = $req->delete("users".'/'.$id);

            $output = json_decode($hasil->getBody(), TRUE);
            if (isset($output['success']) && !empty($output['success'])) {
                echo json_decode($output['success']);
                exit();
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }
    }

    public function rules($id=0, $merge=[]) {
        return array_merge(
            [
                'email'             => 'email|unique:users'.($id? ",id,$id":''),
                'user_tipe'         => 'required',
                'name'              => 'required',
                'jenis_kelamin'     => 'required',
                'pendidikan'        => 'required',
                'phone'             => 'required',
                'alamat'            => 'required',
                'organization_id'   => 'required',
                'status'            => 'required'
            ],
        $merge);
    }
}
