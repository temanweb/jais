<?php

namespace App\Http\Controllers\Admin;

use App\Library\ApiClient;
use App\Library\HttpClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Datatables;
use Illuminate\Support\Collection;

use URL, Input, Redirect, Validator, Session;

class ProfileController extends Controller
{
    private $status = array(''=>'Pilih', '1'=>'Aktif', '2'=>'Tidak Aktif');
    private $jk = array(''=>'Pilih', '1'=>'Pria', '2'=>'Wanita');
    private $didik = array(''=>'Pilih', '1'=>'SMA', '2'=>'S1', '3'=>'S2', '4'=>'S3');
    private $tipe = array(''=>'Pilih', '1'=>'Administrator', '2'=>'Pemantau');

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->id = \Auth::user()->id;
        try {
            $org = new HttpClient();
            $hslorg = $org->get("organization");
            $neworg = json_decode($hslorg->getBody(), TRUE);

            $organisasi = array();
            $organisasi[''] = 'Pilih';
            foreach ($neworg as $k=>$v) {
                $organisasi[$v['id']] = $v['name'];
            }

            $req = new HttpClient();
            $hasil = $req->get("users".'/'.$this->id);

            $output = json_decode($hasil->getBody(), TRUE);

            $data = (object) array(
                'id' => $output['id'],
                'name' => $output['name'],
                'email' => $output['email'],
                'jenis_kelamin' => (isset($output['details']['jenis_kelamin'])) ? $output['details']['jenis_kelamin'] : '',
                'pendidikan' => (isset($output['details']['pendidikan'])) ? $output['details']['pendidikan'] : '',
                'alamat' => (isset($output['details']['alamat'])) ? $output['details']['alamat'] : '',
                'phone' => (isset($output['details']['phone'])) ? $output['details']['phone'] : '',
                'organization_id' => (isset($output['details']['organization_id'])) ? $output['details']['organization_id'] : '',
                'user_tipe' => (isset($output['details']['user_tipe'])) ? $output['details']['user_tipe'] : '',
                'status' => (isset($output['details']['status'])) ? $output['details']['status'] : ''
            );

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        $tipe = $this->tipe;
        $jk = $this->jk;
        $didik = $this->didik;

        return view('admin.profile.index', compact('data', 'organisasi', 'tipe', 'jk', 'didik'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {
            try {
                //print_r (Input::all());
                //die;
                $array = array();
                $array['email'] = $request->email;
                if (isset($request->password) && !empty($request->password)) {
                    $array['password'] = $request->password;
                }
                $array['user_tipe'] = $request->user_tipe;
                $array['name'] = $request->name;
                $array['jenis_kelamin'] = $request->jenis_kelamin;
                $array['pendidikan'] = $request->pendidikan;
                $array['phone'] = $request->phone;
                $array['alamat'] = $request->alamat;
                $array['organization'] = $request->organization_id;
                $array['status'] = $request->status;

                $req = new HttpClient();

                $hasil = $req->put("users".'/'.$id, $array);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/profile');
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function rules($id=0, $merge=[]) {
        return array_merge(
            [
                'user_tipe'         => 'required',
                'name'              => 'required',
                'jenis_kelamin'     => 'required',
                'pendidikan'        => 'required',
                'phone'             => 'required',
                'alamat'            => 'required',
                'organization_id'   => 'required',
                'status'            => 'required'
            ],
        $merge);
    }
}
