<?php

namespace App\Http\Controllers\Admin;

use App\Library\ApiClient;
use App\Library\HttpClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use URL, Input, Redirect, Validator, Session;

class ReportController extends Controller
{
    private $url_token = 'http://apps.jakartaaids.com/oauth/access_token';
    private $lokasi_layanan = 'http://apps.jakartaaids.com/api/lokasi-layanan';
    private $filter = 'http://apps.jakartaaids.com/api/filters';
    private $yearly = 'http://apps.jakartaaids.com/api/report/yearly';
    private $monthly = 'http://apps.jakartaaids.com/api/report/monthly';
    private $tipe = 'http://apps.jakartaaids.com/api/report/type';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$token = $this->token();
        
        try {
            $req = new HttpClient();
            $hasil = $req->get("filters", ['type' => 'kota']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $kota[''] = 'Pilih';
            foreach ($output as $k=>$v) {
                $kota[$v['id']] = $v['name'];
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.report.index', compact('kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(Input::all(), $this->yearrules());

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {

            $bulan = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

            try {
                $req1 = new HttpClient();
                $hasil1 = $req1->get("filters", ['type' => 'kota']);

                $output1 = json_decode($hasil1->getBody(), TRUE);

                $kota = array();
                $kota[''] = 'Pilih';
                foreach ($output1 as $k=>$v) {
                    $kota[$v['id']] = $v['name'];
                }


                $req = new HttpClient();
                $hasil = $req->post("report/yearly", ['kota_id' => $request->kota_id]);

                $output = json_decode($hasil->getBody(), TRUE);

                $table = '<table class="table table-striped table-condensed">';
                if (!empty($output) && is_array($output)) {
                    $namakota = array_keys($output)[0];

                    $table_header = '<thead><tr><th>Jenis / Bulan</th><th>Jan</th><th>Feb</th><th>Mar</th><th>Apr</th><th>Mei</th><th>Jun</th><th>Jul</th><th>Agu</th><th>Sep</th><th>Okt</th><th>Nov</th><th>Des</th></tr></thead>';
                    
                    $hotspot_aktif = array();
                    $hotspot_tidak_aktif = array();
                    $hotspot_baru = array();
                    $perkiraan_populasi = array();
                    $populasi_terjangkau = array();

                    $ha = '<tr><th>Hotspot Aktif</th>';
                    $hta = '<tr><th>Hotspot Tidak Aktif</th>';
                    $hb = '<tr><th>Hotspot Baru</th>';
                    $pp = '<tr><th>Perkiraan Populasi</th>';
                    $pt = '<tr><th>Populasi Terjangkau</th>';
                    foreach ($bulan as $row) {
                        $hotspot_aktif[] = (isset($output[$namakota]['hotspot_aktif'][$row])) ? (int) $output[$namakota]['hotspot_aktif'][$row] : 0;
                        $hotspot_tidak_aktif[] = (isset($output[$namakota]['hotspot_tidak_aktif'][$row])) ? (int) $output[$namakota]['hotspot_tidak_aktif'][$row] : 0;
                        $hotspot_baru[] = (isset($output[$namakota]['hotspot_baru'][$row])) ? (int) $output[$namakota]['hotspot_baru'][$row] : 0;
                        $perkiraan_populasi[] = (isset($output[$namakota]['perkiraan_populasi'][$row])) ? (int) $output[$namakota]['perkiraan_populasi'][$row] : 0;
                        $populasi_terjangkau[] = (isset($output[$namakota]['populasi_terjangkau'][$row])) ? (int) $output[$namakota]['populasi_terjangkau'][$row] : 0;

                        $ha .= (isset($output[$namakota]['hotspot_aktif'][$row])) ? '<td>'.$output[$namakota]['hotspot_aktif'][$row].'</td>' : '<td>0</td>';
                        $hta .= (isset($output[$namakota]['hotspot_tidak_aktif'][$row])) ? '<td>'.$output[$namakota]['hotspot_tidak_aktif'][$row].'</td>' : '<td>0</td>';
                        $hb .= (isset($output[$namakota]['hotspot_baru'][$row])) ? '<td>'.$output[$namakota]['hotspot_baru'][$row].'</td>' : '<td>0</td>';
                        $pp .= (isset($output[$namakota]['perkiraan_populasi'][$row])) ? '<td>'.$output[$namakota]['perkiraan_populasi'][$row].'</td>' : '<td>0</td>';
                        $pt .= (isset($output[$namakota]['populasi_terjangkau'][$row])) ? '<td>'.$output[$namakota]['populasi_terjangkau'][$row].'</td>' : '<td>0</td>';
                    }
                    $ha .= '</tr>';
                    $hta .= '</tr>';
                    $hb .= '</tr>';
                    $pp .= '</tr>';
                    $pt .= '</tr>';

                    $table .= $table_header.$ha.$hta.$hb.$pp.$pt;
                    $table .= '</table>';

                    $yearChart = array();
                    $yearChart["chart"] = array("type" => "spline");
                    $yearChart["legend"] = array("align" => "center", "verticalAlign" => "top", "x" => 0, "y" => 30);
                    $yearChart["title"] = array("text" => "Report Tahunan Kota : " . $namakota);
                    $yearChart["xAxis"] = array("categories" => ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']);
                    $yearChart["yAxis"] = array("title" => array("text" => ""));
                    $yearChart['tooltip'] = array("crosshair" => true, "shared"=> true );
                    $yearChart['credits'] = array("enabled" => false);

                    $yearChart["series"] = [
                        array("name" => "Hotspot Aktif", "marker" => array("symbol" => "circle"), "data" => $hotspot_aktif),
                        array("name" => "Hotspot Tidak Aktif", "marker" => array("symbol" => "circle"), "data" => $hotspot_tidak_aktif),
                        array("name" => "Hotspot Baru", "marker" => array("symbol" => "circle"), "data" => $hotspot_baru),
                        array("name" => "Perkiraan Populasi", "marker" => array("symbol" => "circle"), "data" => $perkiraan_populasi),
                        array("name" => "Populasi Terjangkau", "marker" => array("symbol" => "circle"), "data" => $populasi_terjangkau)
                    ];
                } else {

                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }

        return view('admin.report.index', compact('kota', 'yearChart', 'namakota', 'table'));
    }


    public function monthly() {
        $bulan = array(''=>'Pilih', '1'=>'Januari', '2'=>'Februari', '3'=>'Maret', '4'=>'April', '5'=>'Mei', '6'=>'Juni', '7'=>'Juli', '8'=>'Agustus', '9'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember');

        return view('admin.report.month', compact('bulan'));
    }

    public function chartmonthly(Request $request) {
        $validator = Validator::make(Input::all(), $this->monthrules());

        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {

            $bulan = array(''=>'Pilih', '1'=>'January', '2'=>'February', '3'=>'March', '4'=>'April', '5'=>'May', '6'=>'June', '7'=>'July', '8'=>'August', '9'=>'September', '10'=>'October', '11'=>'November', '12'=>'December');
            $jenis = array('hotspot_aktif', 'hotspot_tidak_aktif', 'hotspot_baru', 'estimasi_populasi', 'populasi_terjangkau');

            try {
                $req = new HttpClient();
                $hasil = $req->post("report/monthly", ['bulan' => $request->bulan]);

                $output = json_decode($hasil->getBody(), TRUE);

                $table = '<table class="table table-striped table-condensed">';
                if (!empty($output) && is_array($output)) {
                    $namabulan = array_keys($output)[0];

                    $table_header = '';

                    $hotspot_aktif = array();
                    $merged = array();
                    $kota = '';
                    $city = array();
                    $i = 0;
                    foreach ($jenis as $row) {
                        foreach ($output[$namabulan][0] as $k=>$v) {
                            if ($i < count($output[$namabulan][0])) {
                                $kota .= '<th>'.array_keys($v)[0].'</th>';
                                $city[$i] = array_keys($v)[0];
                            }
                            foreach ($v as $bold) {
                                $total = array('total' => $bold[$row]);
                                $merged[$row][] = $bold[$row];
                            }
                        $i++;
                        }
                    }

                    $table_header = '<thead><tr><th>Jenis / Kota</th>'.$kota.'</tr></thead>';

                    $ha = '<tr><th>Hotspot Aktif</th>';
                    $hta = '<tr><th>Hotspot Tidak Aktif</th>';
                    $hb = '<tr><th>Hotspot Baru</th>';
                    $pp = '<tr><th>Perkiraan Populasi</th>';
                    $pt = '<tr><th>Populasi Terjangkau</th>';

                    foreach ($merged['hotspot_aktif'] as $r1) { $ha .= '<td>'.$r1.'</td>'; }
                    foreach ($merged['hotspot_tidak_aktif'] as $r2) { $hta .= '<td>'.$r2.'</td>'; }
                    foreach ($merged['hotspot_baru'] as $r3) { $hb .= '<td>'.$r3.'</td>'; }
                    foreach ($merged['estimasi_populasi'] as $r4) { $pp .= '<td>'.$r4.'</td>'; }
                    foreach ($merged['populasi_terjangkau'] as $r5) { $pt .= '<td>'.$r5.'</td>'; }

                    $ha .= '</tr>';
                    $hta .= '</tr>';
                    $hb .= '</tr>';
                    $pp .= '</tr>';
                    $pt .= '</tr>';

                    $table .= $table_header.$ha.$hta.$hb.$pp.$pt;
                    $table .= '</table>';

                    $monthChart = array();
                    $monthChart["chart"] = array("type" => "spline");
                    $monthChart["legend"] = array("align" => "center", "verticalAlign" => "top", "x" => 0, "y" => 30);
                    $monthChart["title"] = array("text" => "Report Bulan : " . $namabulan);
                    $monthChart["xAxis"] = array("categories" => $city);
                    $monthChart["yAxis"] = array("title" => array("text" => ""));
                    $monthChart['tooltip'] = array("crosshair" => true, "shared"=> true );
                    $monthChart['credits'] = array("enabled" => false);

                    $monthChart["series"] = [
                        array("name" => "Hotspot Aktif", "marker" => array("symbol" => "circle"), "data" => $merged['hotspot_aktif']),
                        array("name" => "Hotspot Tidak Aktif", "marker" => array("symbol" => "circle"), "data" => $merged['hotspot_tidak_aktif']),
                        array("name" => "Hotspot Baru", "marker" => array("symbol" => "circle"), "data" => $merged['hotspot_baru']),
                        array("name" => "Perkiraan Populasi", "marker" => array("symbol" => "circle"), "data" => $merged['estimasi_populasi']),
                        array("name" => "Populasi Terjangkau", "marker" => array("symbol" => "circle"), "data" => $merged['populasi_terjangkau'])
                    ];
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }

        return view('admin.report.month', compact('bulan', 'monthChart', 'namabulan', 'table'));
    }

    public function active() {
        $bulan = array('January'=>0, 'February'=>0, 'March'=>0, 'April'=>0, 'May'=>0, 'June'=>0, 'July'=>0, 'August'=>0, 'September'=>0, 'October'=>0, 'November'=>0, 'December'=>0);
        $moon = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des');

        try {
            $req = new HttpClient();
            $hasil = $req->post("report/type", ['type' => 'hotspot_aktif']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $test = array();

            $i = 0;
            foreach ($output['hotspot_aktif'] as $row) {
                $kota['kota'][$i] = array_keys($row)[0];

                foreach ($row as $r) {
                    if (is_array($r[0]) && !empty($r[0])) {
                        $total = array_merge($bulan, $r[0]);
                        $kota['total'][$i] = array_values($total);
                    } else {
                        $total = $bulan;
                        $kota['total'][$i] = array_values($total);
                    }
                }
                $i++;
            }

            $table = '<table class="table table-striped table-condensed">';
            $table .= '<thead><tr><th>Kota / Bulan</th>';
            foreach ($moon as $mon) {
                $table .= '<th>'.$mon.'</th>';
            }
            $table .= '</tr></thead>';
            $array = array();
            for ($i=0; $i<count($kota['kota']); $i++) {
                $array[] = array("name" => $kota['kota'][$i], "marker" => array("symbol" => "circle"), "data" => $kota['total'][$i]);
                $table .= '<tr><td>'.$kota['kota'][$i].'</td>';
                foreach ($kota['total'][$i] as $rows) {
                    $table .= '<td>'.$rows.'</td>';
                }
                $table .= '</tr>';
            }
            $table .= '</table>';

            $typeChart = array();
            $typeChart["chart"] = array("type" => "spline");
            $typeChart["legend"] = array("align" => "center", "verticalAlign" => "top", "x" => 0, "y" => 30);
            $typeChart["title"] = array("text" => "Report Hotspot Aktif");
            $typeChart["xAxis"] = array("categories" => $moon);
            $typeChart["yAxis"] = array("title" => array("text" => ""));
            $typeChart['tooltip'] = array("crosshair" => true, "shared"=> true );
            $typeChart['credits'] = array("enabled" => false);
            $typeChart["series"] = $array;

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.report.active', compact('typeChart', 'table'));
    }

    public function nonactive() {
        $bulan = array('January'=>0, 'February'=>0, 'March'=>0, 'April'=>0, 'May'=>0, 'June'=>0, 'July'=>0, 'August'=>0, 'September'=>0, 'October'=>0, 'November'=>0, 'December'=>0);
        $moon = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des');

        try {
            $req = new HttpClient();
            $hasil = $req->post("report/type", ['type' => 'hotspot_tidak_aktif']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $test = array();

            $i = 0;
            foreach ($output['hotspot_tidak_aktif'] as $row) {
                $kota['kota'][$i] = array_keys($row)[0];

                foreach ($row as $r) {
                    if (is_array($r[0]) && !empty($r[0])) {
                        $total = array_merge($bulan, $r[0]);
                        $kota['total'][$i] = array_values($total);
                    } else {
                        $total = $bulan;
                        $kota['total'][$i] = array_values($total);
                    }
                }
                $i++;
            }

            $table = '<table class="table table-striped table-condensed">';
            $table .= '<thead><tr><th>Kota / Bulan</th>';
            foreach ($moon as $mon) {
                $table .= '<th>'.$mon.'</th>';
            }
            $table .= '</tr></thead>';
            $array = array();
            for ($i=0; $i<count($kota['kota']); $i++) {
                $array[] = array("name" => $kota['kota'][$i], "marker" => array("symbol" => "circle"), "data" => $kota['total'][$i]);
                $table .= '<tr><td>'.$kota['kota'][$i].'</td>';
                foreach ($kota['total'][$i] as $rows) {
                    $table .= '<td>'.$rows.'</td>';
                }
                $table .= '</tr>';
            }
            $table .= '</table>';

            $typeChart = array();
            $typeChart["chart"] = array("type" => "spline");
            $typeChart["legend"] = array("align" => "center", "verticalAlign" => "top", "x" => 0, "y" => 30);
            $typeChart["title"] = array("text" => "Report Hotspot Tidak Aktif");
            $typeChart["xAxis"] = array("categories" => $moon);
            $typeChart["yAxis"] = array("title" => array("text" => ""));
            $typeChart['tooltip'] = array("crosshair" => true, "shared"=> true );
            $typeChart['credits'] = array("enabled" => false);
            $typeChart["series"] = $array;

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.report.nonactive', compact('typeChart', 'table'));
    }

    public function hotspotnew() {
        $bulan = array('January'=>0, 'February'=>0, 'March'=>0, 'April'=>0, 'May'=>0, 'June'=>0, 'July'=>0, 'August'=>0, 'September'=>0, 'October'=>0, 'November'=>0, 'December'=>0);
        $moon = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des');

        try {
            $req = new HttpClient();
            $hasil = $req->post("report/type", ['type' => 'hotspot_baru']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $test = array();

            $i = 0;
            foreach ($output['hotspot_baru'] as $row) {
                $kota['kota'][$i] = array_keys($row)[0];

                foreach ($row as $r) {
                    if (is_array($r[0]) && !empty($r[0])) {
                        $total = array_merge($bulan, $r[0]);
                        $kota['total'][$i] = array_values($total);
                    } else {
                        $total = $bulan;
                        $kota['total'][$i] = array_values($total);
                    }
                }
                $i++;
            }

            $table = '<table class="table table-striped table-condensed">';
            $table .= '<thead><tr><th>Kota / Bulan</th>';
            foreach ($moon as $mon) {
                $table .= '<th>'.$mon.'</th>';
            }
            $table .= '</tr></thead>';
            $array = array();
            for ($i=0; $i<count($kota['kota']); $i++) {
                $array[] = array("name" => $kota['kota'][$i], "marker" => array("symbol" => "circle"), "data" => $kota['total'][$i]);
                $table .= '<tr><td>'.$kota['kota'][$i].'</td>';
                foreach ($kota['total'][$i] as $rows) {
                    $table .= '<td>'.$rows.'</td>';
                }
                $table .= '</tr>';
            }
            $table .= '</table>';

            $typeChart = array();
            $typeChart["chart"] = array("type" => "spline");
            $typeChart["legend"] = array("align" => "center", "verticalAlign" => "top", "x" => 0, "y" => 30);
            $typeChart["title"] = array("text" => "Report Hotspot Baru");
            $typeChart["xAxis"] = array("categories" => $moon);
            $typeChart["yAxis"] = array("title" => array("text" => ""));
            $typeChart['tooltip'] = array("crosshair" => true, "shared"=> true );
            $typeChart['credits'] = array("enabled" => false);
            $typeChart["series"] = $array;

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.report.new', compact('typeChart', 'table'));
    }

    public function populasi() {
        $bulan = array('January'=>0, 'February'=>0, 'March'=>0, 'April'=>0, 'May'=>0, 'June'=>0, 'July'=>0, 'August'=>0, 'September'=>0, 'October'=>0, 'November'=>0, 'December'=>0);
        $moon = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des');

        try {
            $req = new HttpClient();
            $hasil = $req->post("report/type", ['type' => 'populasi_terjangkau']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $test = array();

            $i = 0;
            foreach ($output['populasi_terjangkau'] as $row) {
                $kota['kota'][$i] = array_keys($row)[0];

                foreach ($row as $r) {
                    if (is_array($r[0]) && !empty($r[0])) {
                        $total = array_merge($bulan, $r[0]);
                        $kota['total'][$i] = array_values($total);
                    } else {
                        $total = $bulan;
                        $kota['total'][$i] = array_values($total);
                    }
                }
                $i++;
            }

            $table = '<table class="table table-striped table-condensed">';
            $table .= '<thead><tr><th>Kota / Bulan</th>';
            foreach ($moon as $mon) {
                $table .= '<th>'.$mon.'</th>';
            }
            $table .= '</tr></thead>';
            $array = array();
            for ($i=0; $i<count($kota['kota']); $i++) {
                $array[] = array("name" => $kota['kota'][$i], "marker" => array("symbol" => "circle"), "data" => $kota['total'][$i]);
                $table .= '<tr><td>'.$kota['kota'][$i].'</td>';
                foreach ($kota['total'][$i] as $rows) {
                    $table .= '<td>'.$rows.'</td>';
                }
                $table .= '</tr>';
            }
            $table .= '</table>';

            $typeChart = array();
            $typeChart["chart"] = array("type" => "spline");
            $typeChart["legend"] = array("align" => "center", "verticalAlign" => "top", "x" => 0, "y" => 30);
            $typeChart["title"] = array("text" => "Report Populasi Terjangkau");
            $typeChart["xAxis"] = array("categories" => $moon);
            $typeChart["yAxis"] = array("title" => array("text" => ""));
            $typeChart['tooltip'] = array("crosshair" => true, "shared"=> true );
            $typeChart['credits'] = array("enabled" => false);
            $typeChart["series"] = $array;

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.report.populasi', compact('typeChart', 'table'));
    }

    public function estimasi() {
        $bulan = array('January'=>0, 'February'=>0, 'March'=>0, 'April'=>0, 'May'=>0, 'June'=>0, 'July'=>0, 'August'=>0, 'September'=>0, 'October'=>0, 'November'=>0, 'December'=>0);
        $moon = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des');

        try {
            $req = new HttpClient();
            $hasil = $req->post("report/type", ['type' => 'estimasi_populasi']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $test = array();

            $i = 0;
            foreach ($output['estimasi_populasi'] as $row) {
                $kota['kota'][$i] = array_keys($row)[0];

                foreach ($row as $r) {
                    if (is_array($r[0]) && !empty($r[0])) {
                        $total = array_merge($bulan, $r[0]);
                        $kota['total'][$i] = array_values($total);
                    } else {
                        $total = $bulan;
                        $kota['total'][$i] = array_values($total);
                    }
                }
                $i++;
            }

            $table = '<table class="table table-striped table-condensed">';
            $table .= '<thead><tr><th>Kota / Bulan</th>';
            foreach ($moon as $mon) {
                $table .= '<th>'.$mon.'</th>';
            }
            $table .= '</tr></thead>';
            $array = array();
            for ($i=0; $i<count($kota['kota']); $i++) {
                $array[] = array("name" => $kota['kota'][$i], "marker" => array("symbol" => "circle"), "data" => $kota['total'][$i]);
                $table .= '<tr><td>'.$kota['kota'][$i].'</td>';
                foreach ($kota['total'][$i] as $rows) {
                    $table .= '<td>'.$rows.'</td>';
                }
                $table .= '</tr>';
            }
            $table .= '</table>';

            $typeChart = array();
            $typeChart["chart"] = array("type" => "spline");
            $typeChart["legend"] = array("align" => "center", "verticalAlign" => "top", "x" => 0, "y" => 30);
            $typeChart["title"] = array("text" => "Report Perkiraan Populasi");
            $typeChart["xAxis"] = array("categories" => $moon);
            $typeChart["yAxis"] = array("title" => array("text" => ""));
            $typeChart['tooltip'] = array("crosshair" => true, "shared"=> true );
            $typeChart['credits'] = array("enabled" => false);
            $typeChart["series"] = $array;

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.report.estimasi', compact('typeChart', 'table'));
    }

    public function yearrules($id=0, $merge=[]) {
        return array_merge(
            [
                'kota_id'       => 'required'
            ],
        $merge);
    }

    public function monthrules($id=0, $merge=[]) {
        return array_merge(
            [
                'bulan'       => 'required'
            ],
        $merge);
    }
}
