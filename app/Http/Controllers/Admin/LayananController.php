<?php

namespace App\Http\Controllers\Admin;

use App\Library\ApiClient;
use App\Library\HttpClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
//use LucaDegasperi\OAuth2Server\Facades\Authorizer;

use Datatables;
use Illuminate\Support\Collection;

use App\Layanan;
use Carbon\Carbon;
use App\User;

use URL, Input, Redirect, Validator, Session;

class LayananController extends Controller
{
	private $url_token = 'http://apps.jakartaaids.com/oauth/access_token';
	private $lokasi_layanan = 'http://apps.jakartaaids.com/api/lokasi-layanan';
	private $filter = 'http://apps.jakartaaids.com/api/filters';
    private $layanan = array('1'=>'HIV', '2'=>'IMS', '3'=>'PITC', '4'=>'LASS', '5'=>'PTRM', '6'=>'PMTCT', '7'=>'VCT');


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$test = Authorizer::getResourceOwnerId();
        //print_R ($test);
        return view('admin.layanan.index');
    }

    public function getData()
    {
        //$token = $this->token();
        
        try {
            $pandu = new HttpClient();
            $hotspot = $pandu->get("lokasi-layanan/all");

            $output = json_decode($hotspot->getBody(), TRUE);

            $hasil = new Collection();

            foreach ($output as $k=>$v) {
            	$layanan = '';
            	if (isset($v['tipe_layanan']) && !empty($v['tipe_layanan'])) {
            		foreach ($v['tipe_layanan'] as $row) {
            			$layanan .= $row['name'].', ';
            		}
            		$layanan = substr($layanan, 0, -2);
            	}
                $hasil->push([
                    'id' => $v['id'],
                    'name' => $v['name'],
                    'layanan' => $layanan,
                    'latitude' => $v['latitude'],
                    'longtitude' => $v['longtitude'],
                    'address' => $v['address'],
                    'description' => $v['description'],
                    'kota_id' => $v['kota']['name'],
                    'action' => '
                        <button class="btn btn-success btn-xs btn-view" title="view"><i class="fa fa-plus-square"></i></button>
                        <a href="'.URL::to('admin/layanan').'/'.$v['id'].'/edit" class="btn btn-primary btn-xs" title="edit"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs btn-delete" data-remote="/admin/layanan/'.$v['id'].'" title="Hapus"><i class="fa fa-times"></i></a>
                    '
                ]);
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return Datatables::collection($hasil)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        try {
            $req = new HttpClient();
            $hasil = $req->get("filters", ['type' => 'kota']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $kota[''] = 'Pilih';
            foreach ($output as $k=>$v) {
                $kota[$v['id']] = $v['name'];
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        return view('admin.layanan.create', compact('kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());
        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {

            try {
                $req = new HttpClient();
                $hasil = $req->post("lokasi-layanan", [
                    'tipelayanan[0]' => (isset($request->jenis[0])) ? $request->jenis[0] : '',
                    'tipelayanan[1]' => (isset($request->jenis[1])) ? $request->jenis[1] : '',
                    'tipelayanan[2]' => (isset($request->jenis[2])) ? $request->jenis[2] : '',
                    'tipelayanan[3]' => (isset($request->jenis[3])) ? $request->jenis[3] : '',
                    'tipelayanan[4]' => (isset($request->jenis[4])) ? $request->jenis[4] : '',
                    'tipelayanan[5]' => (isset($request->jenis[5])) ? $request->jenis[5] : '',
                    'tipelayanan[6]' => (isset($request->jenis[6])) ? $request->jenis[6] : '',
                    'name' => $request->name,
                    'kota_id' => $request->kota_id,
                    'address' => $request->address,
                    'longtitude' => $request->longitude,
                    'latitude' => $request->latitude,
                    'description' => $request->description
                ]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/layanan');
                }

                $kota = array();
                $kota[''] = 'Pilih';
                foreach ($output as $k=>$v) {
                    $kota[$v['id']] = $v['name'];
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {
            $req = new HttpClient();
            $hasil = $req->get("filters", ['type' => 'kota']);

            $output = json_decode($hasil->getBody(), TRUE);

            $kota = array();
            $kota[''] = 'Pilih';
            foreach ($output as $k=>$v) {
                $kota[$v['id']] = $v['name'];
            }

            $newreq = new HttpClient();
            $newhsl = $newreq->get("lokasi-layanan".'/'.$id.'/edit');

            $newout = json_decode($newhsl->getBody(), TRUE);

            if (!empty($newout)) {
                if (isset($newout['tipe_layanan']) && !empty($newout['tipe_layanan'])) {
                    $layan = array();
                    foreach ($newout['tipe_layanan'] as $k=>$v) {
                        $layan[] = $v['id'];
                    }
                }
                //$layan = array('1', '2');

                $data = (object) array(
                    'id' => $newout['id'],
                    'name' => $newout['name'],
                    'latitude' => $newout['latitude'],
                    'longitude' => $newout['longtitude'],
                    'address' => $newout['address'],
                    'description' => $newout['description'],
                    'kota_id' => $newout['kota_id'],
                    'jenis' => $layan
                );
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }

        $listlayanan = $this->layanan;

        return view('admin.layanan.edit', compact('kota', 'data', 'listlayanan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make(Input::all(), $this->rules());
        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        } else {

            try {
                $req = new HttpClient();
                $hasil = $req->put("lokasi-layanan".'/'.$id, [
                    'tipelayanan[0]' => (isset($request->jenis[0])) ? $request->jenis[0] : '',
                    'tipelayanan[1]' => (isset($request->jenis[1])) ? $request->jenis[1] : '',
                    'tipelayanan[2]' => (isset($request->jenis[2])) ? $request->jenis[2] : '',
                    'tipelayanan[3]' => (isset($request->jenis[3])) ? $request->jenis[3] : '',
                    'tipelayanan[4]' => (isset($request->jenis[4])) ? $request->jenis[4] : '',
                    'tipelayanan[5]' => (isset($request->jenis[5])) ? $request->jenis[5] : '',
                    'tipelayanan[6]' => (isset($request->jenis[6])) ? $request->jenis[6] : '',
                    'name' => $request->name,
                    'kota_id' => $request->kota_id,
                    'address' => $request->address,
                    'longtitude' => $request->longitude,
                    'latitude' => $request->latitude,
                    'description' => $request->description
                ]);

                $output = json_decode($hasil->getBody(), TRUE);
                if (isset($output['success']) && !empty($output['success'])) {
                    Session::flash('flash_message', $output['success']);
                    return Redirect::to('admin/layanan');
                }

                $kota = array();
                $kota[''] = 'Pilih';
                foreach ($output as $k=>$v) {
                    $kota[$v['id']] = $v['name'];
                }

            } catch (\GuzzleHttp\Exception\RequestException $e) {
                return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //$token = $this->token();
            
        try {
            $req = new HttpClient();
            $hasil = $req->delete("lokasi-layanan".'/'.$id);

            $output = json_decode($hasil->getBody(), TRUE);
            if (isset($output['success']) && !empty($output['success'])) {
                echo json_decode($output['success']);
                exit();
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return Redirect::back()->withErrors('Ada masalah koneski. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return Redirect::back()->withErrors('Ada masalah koneksi. Silahkan coba beberapa saat lagi.');
        }
    }

    public function rules($id=0, $merge=[]) {
        return array_merge(
            [
                'name'          => 'required',
                'kota_id'       => 'required',
                'address'       => 'required',
                'jenis'         => 'required',
                'longitude'     => 'required',
                'latitude'      => 'required',
                'description'   => 'required'
            ],
        $merge);
    }

}
