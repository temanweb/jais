<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporanhotspot extends Model
{
    //
    protected $table = "laporan_hotspot";

    protected $fillable = [
    	'hotspot_id',
    	'estimasi_populasi',
    	'populasi_berpindah',
    	'populasi_baru',
    	'populasi_terjangkau',
    	'status',
    	'notes',
    	'user_id',
    	'created_at',
    	'updated_at'
    ];
}
