<?php
/**
 * Created by PhpStorm.
 * User: denipermana
 * Date: 5/31/16
 * Time: 1:58 PM
 */

namespace App\Auth;


use App\Library\ApiClient;
use App\Library\HttpClient;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CustomUserProvider implements UserProvider
{

    private $client;
    private $user;

    /**
     * CustomUserProvider constructor.
     */
    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('app.api_url')]);

    }

    public function retrieveById($identifier)
    {
        $session = Session::get("user");
        if($session->id == $identifier){
            return $session;
        }
        return null;
    }

    public function retrieveByToken($identifier, $token)
    {

        // TODO: Implement retrieveByToken() method.
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        // TODO: Implement updateRememberToken() method.

    }

    public function retrieveByCredentials(array $credentials)
    {

//        if(!Cache::has('access_token')) {
            $this->login($credentials);
            return new User($this->getUser());
//        }
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        // TODO: Implement validateCredentials() method.
        $session = Session::get("user");
        return Hash::check('password', $session->password );
    }

    public function login($credentials)
    {
        ApiClient::instantiate($credentials['email'], $credentials['password']);
        $this->setSession();
//        $params = ['form_params' =>
//            [
//                'client_id' => 1,
//                'client_secret' => '226655d5e7bf98a0d31602557d23f194',
//                'grant_type' => 'password',
//                'username' => $credentials['email'],
//                'password' => $credentials['password']
//            ]
//        ];
//        try{
//            $token = $this->client->request("POST", 'oauth/access_token', $params);
//            $result = json_decode($token->getBody());
//            $this->saveToken($result);
//
//        }catch (\Exception $e){
//            die($e->getMessage());
//            $responseBody = $e->getResponse()->getBody(true);
//            return null;
//        }

    }
    public function saveToken($token)
    {
        if(!Cache::has('access_token')){
            Cache::put('access_token', $token->access_token, $token->expires_in - 100);
            Cache::put('refresh_token', $token->refresh_token, $token->expires_in - 100);
        }


    }

    public function setSession()
    {
        $httpClient = new HttpClient();
        $result = $httpClient->get("user/profile");
        Session::put('user', json_decode($result->getBody()));
        Session::save();
        $userData = json_decode($result->getBody());
        $user = [
            'id' => $userData->id,
            'email' => $userData->email,
            'name' => $userData->name,
            'password' => $userData->password
        ];

        $this->setUser($user);
        return $user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function updateSession()
    {

    }

    public function removeSession()
    {

    }

}