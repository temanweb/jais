<?php
namespace App\Classes;

class Common {

    /* List Status */
    public function listStatus() {
        return array(''=>'Pilih', '1'=>'Aktif', '2'=>'Tidak Aktif');
    }

    /* Jenis Kelamin */
    public function jenisKelamin() {
        return array(''=>'Pilih', '1'=>'Pria', '2'=>'Wanita');
    }

    /* Pendidikan */
    public function pendidikan() {
        return array(''=>'Pilih', '1'=>'TK', '2'=>'SD', '3'=>'SMP', '4'=>'SMA', '5'=>'D1', '6'=>'D2', '7'=>'D3', '8'=>'S1', '9'=>'S2', '10'=>'S3');
    }

    /* Jenis User */
    public function tipeUser() {
        return array(''=>'Pilih', '1'=>'Administrator', '2'=>'Member');
    }

    public function tipeHotspot() {
        return array(''=>'Pilih', '1'=>'Bar', '2'=>'Cafe', '3'=>'Jalan', '4'=>'Lainnya', '5'=>'Panti Pijat', '6'=>'Rumah Kost', '7'=>'Wisma', '8'=>'Warung', '9'=>'Minimart', '10'=>'Hotel', '11'=>'Karaoke', '12'=>'Taman', '13'=>'Spa', '14'=>'Stasiun', '15'=>'Mall');
    }

}